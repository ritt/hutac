function rpc_readtemp()
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         document.getElementById("tempAir").innerHTML = o.result[0];
         document.getElementById("tempHut").innerHTML = o.result[1];
         document.getElementById("tempLM35").innerHTML = o.result[2];
         setTimeout(rpc_readtemp, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "readtemp";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_readValve() {
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         document.getElementById("valveStatus").innerHTML = o.valve;
         setTimeout(rpc_readValve, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "readValve";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_readPID()
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         if (o.airActive) {
            document.getElementById("AirStatus").innerHTML ="Active";
         } else {
            document.getElementById("AirStatus").innerHTML ="inactive";
         }
         if (o.hutActive) {
            document.getElementById("HutStatus").innerHTML ="Active";
         } else {
            document.getElementById("HutStatus").innerHTML ="inactive";
         }
         document.getElementById("GetAirReqT").innerHTML = o.airReqTemp;
         document.getElementById("GetHutReqT").innerHTML = o.hutReqTemp;
         
         setTimeout(rpc_readPID, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "readpid";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setTemp()
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         //document.getElementById("result").innerHTML = "Result: "+o.result;
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setTemp";
   request.params = [];
   request.params[0] = 1;
   request.params[1] = document.getElementById("SetHutTemp").value;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setactive(status)
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setAllActive";
   request.params = [];
   request.params[0] = status;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

