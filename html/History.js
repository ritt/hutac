var gHistory = {
   minTime : 0,
   maxTime : 1,
   minTemp : 0,
   maxTemp : 1,
   nEntries : 0,
   entries : [[]],
   fontSize : 12,
   isPaused : false,
   timeSpan : 0
};

var gLinkBox = {
   linksTo : History.html,
   width : 0,
   height : 0,
   xpos : 0,
   ypos : 0,
   isActive : false,
   maximise : true
};

var gMouse = {
   isOverActiveArea : 0,
   isPushed : 0,
   xRel : 0,
   xAbs : 0,
   yAbs : 0,
   xAbsPush : 0,
   xAbsRelease : 0
};

function readCookie() {
   var t1 = 0;
   var t2 = 0;
   var timeSpan = 600;
   
   var cookie = decodeURIComponent(document.cookie).split(';');
   for (var i = 0; i < cookie.length; ++i) {
      var c = cookie[i];
      while (c.charAt(0) == ' ') {
         c = c.substring(1);
      }
      if (c.indexOf("t1=") == 0) {
         t1 = parseInt(c.split('=')[1]);
      } else if (c.indexOf("t2=") == 0) {
         t2 = parseInt(c.split('=')[1]);
      } else if (c.indexOf("timeSpan=") == 0) {
         timeSpan = parseInt(c.split('=')[1]);
      }
   }
   
   gHistory.timeSpan = timeSpan;
   
   var radios = document.getElementsByName("time");
   for (var i = 0; i < radios.length; ++i) {
      if ( gHistory.timeSpan == radios[i].value) {
         radios[i].checked = "checked";
      }
   }
   gHistory.minTime = Math.min(t1, t2);
   gHistory.maxTime = Math.max(t1, t2);
}

function canvasMouseMove(event) {
   var canv = document.getElementById("cHistory");
   var xRel = (event.offsetX / canv.width -0.1) / 0.8 ;
   if (event.offsetY < 0.9 * canv.height &&
       event.offsetY > 0.1 * canv.height &&
       xRel < 1 && xRel > 0) {
      //An event inside the main plot
      gMouse.isOverActiveArea = true;
   } else {
      gMouse.isOverActiveArea = false;
   }
   if (event.offsetY > gLinkBox.ypos && event.offsetY < gLinkBox.ypos + gLinkBox.height && event.offsetX > gLinkBox.xpos && event.offsetX < gLinkBox.xpos + gLinkBox.width) {
      if (!event.buttons) {
         gLinkBox.isActive = true;
      }
   } else {
      gLinkBox.isActive = false;
   }
   gMouse.xRel = xRel;
   gMouse.xAbs = event.offsetX;
   gMouse.yAbs = event.offsetY;
   drawHistory();
}

function canvasMousePush(event) {
   var canv = document.getElementById("cHistory");
   var xRel = (event.offsetX / canv.width -0.1) / 0.8 ;
   if (event.offsetY < 0.9 * canv.height &&
       event.offsetY > 0.1 * canv.height &&
       xRel < 1 && xRel > 0 && !gMouse.isPushed && !gLinkBox.isActive) {
      //An event inside the main plot
      gMouse.xAbsPush = event.offsetX;
      gMouse.isPushed = true;
   }
}

function canvasMouseRelease(event) {
   if (gMouse.isPushed) {
      gMouse.xAbsRelease = event.offsetX;
      gMouse.isPushed = false;
      var canv = document.getElementById("cHistory");
      var t1 = toAbsValue((gMouse.xAbsPush / canv.width - 0.1) / 0.8);
      var t2 = toAbsValue((gMouse.xAbsRelease / canv.width - 0.1) / 0.8);
      rpc_requestzoom(t1, t2);
      drawHistory();
   }
   if (gLinkBox.isActive) {
      window.location = gLinkBox.linksTo;
   }
}

function canvasMouseOut(event) {
   gMouse.isPushed = 0;
   drawHistory();
}

function canvasTouchStart(event) {
   var canv = document.getElementById("cHistory");
   var touchX = event.touches[0].pageX - event.touches[0].target.offsetLeft;
   var touchY = event.touches[0].pageY - event.touches[0].target.offsetTop;
   
   var xRel = (touchX / canv.width -0.1) / 0.8 ;
   if (touchY > 0.9 * canv.height &&
       xRel < 1 && xRel > 0 && !gMouse.isPushed) {
      //An event inside the main plot
      gMouse.xAbsPush = touchX;
      gMouse.xAbs = touchX;
      gMouse.isPushed = true;
   }
}

function canvasTouchMove(event) {
   var canv = document.getElementById("cHistory");
   var touchX = event.touches[0].pageX - event.touches[0].target.offsetLeft;
   var touchY = event.touches[0].pageY - event.touches[0].target.offsetTop;
   var xRel = (touchX / canv.width -0.1) / 0.8 ;
   if (touchY < 0.9 * canv.height &&
       touchY > 0.1 * canv.height &&
       xRel < 1 && xRel > 0) {
      //An event inside the main plot
      gMouse.isOverActiveArea = true;
   } else {
      gMouse.isOverActiveArea = false;
   }
   if (touchY > gLinkBox.ypos &&
       touchY < gLinkBox.ypos + gLinkBox.height &&
       touchX > gLinkBox.xpos &&
       touchX < gLinkBox.xpos + gLinkBox.width) {
      gLinkBox.isActive = true;
   } else {
      gLinkBox.isActive = false;
   }
   if (touchY < 0.9 * canv.height) {
      gMouse.isPushed = false;
   }
   gMouse.xRel = xRel;
   gMouse.xAbs = touchX;
   gMouse.yAbs = touchY;
   drawHistory();
}

function canvasTouchEnd(event) {
   var canv = document.getElementById("cHistory");
   if (gMouse.isPushed) {
      gMouse.xAbsRelease = gMouse.xAbs;
      gMouse.isPushed = false;
      
      var dx = Math.abs(gMouse.xAbsPush - gMouse.xAbsRelease);
      console.log(dx);
      if (dx > 0.1 * canv.width) {
         var t1 = toAbsValue((gMouse.xAbsPush / canv.width - 0.1) / 0.8);
         var t2 = toAbsValue((gMouse.xAbsRelease / canv.width - 0.1) / 0.8);
         rpc_requestzoom(t1, t2);
      } else if (dx < 0.05) {
         rpc_requestzoom(0,0);
      }
      drawHistory();
   }
}

function toAbsValue(x, axis) {
   var res
   if (axis) {
      //y-axis, temperature
      res = gHistory.minTemp + x * (gHistory.maxTemp - gHistory.minTemp);
   } else {
      //x-axis, time
      res = gHistory.minTime + x * (gHistory.maxTime - gHistory.minTime);
   }
   return res;
}

function drawLinkBox() {
   var canv = document.getElementById("cHistory");
   var ctx = canv.getContext("2d");
   
   var x = gLinkBox.xpos;
   var y = gLinkBox.ypos;
   var w = gLinkBox.width;
   var h = gLinkBox.height;
   ctx.save();
   ctx.beginPath();
   ctx.strokeStyle = "gray";
   ctx.fillStyle = "lightgray";
   
   var r1 = 0.15;
   var r2 = 0.4;
   
   
   ctx.fillRect(x, y, w, h);
   ctx.strokeRect(x, y, w, h);
   ctx.beginPath();
   ctx.strokeStyle = "black";
   ctx.lineWidth = 2;
   ctx.moveTo(x + r1 * w, y + r2 * h);
   if (gLinkBox.maximise) {
      ctx.lineTo(x + r1 * w, y + r1 * h);
   } else {
      ctx.lineTo(x + r2 * w, y + r2 * h);
   }
   ctx.lineTo(x + r2 * w, y + r1 * h);
   
   ctx.moveTo(x + r1 * w, y + (1 - r2) * h);
   if (gLinkBox.maximise) {
      ctx.lineTo(x + r1 * w, y + (1 - r1) * h);
   } else {
      ctx.lineTo(x + r2 * w, y + (1 - r2) * h);
   }
   ctx.lineTo(x + r2 * w, y + (1 - r1) * h);
   
   ctx.moveTo(x + (1 - r2) * w, y + (1 - r1) * h);
   if (gLinkBox.maximise) {
      ctx.lineTo(x + (1 - r1) * w, y + (1 - r1) * h);
   } else {
      ctx.lineTo(x + (1 - r2) * w, y + (1 - r2) * h);
   }
   ctx.lineTo(x + (1 - r1)* w, y + (1 - r2) * h);
   
   ctx.moveTo(x + (1 - r1) * w, y + r2 * h);
   if (gLinkBox.maximise) {
      ctx.lineTo(x + (1 - r1) * w, y + r1 * h);
   } else {
      ctx.lineTo(x + (1 - r2) * w, y + r2 * h);
   }
   ctx.lineTo(x + (1 - r2) * w, y + r1 * h);
   ctx.stroke();
   
   ctx.restore();
}

function drawMouseOver() {
   var diffClosest = 10;
   var iClosest = -1;
   
   for (var iEntry = 0; iEntry < gHistory.nEntries; ++iEntry) {
      var diff = Math.abs(gHistory.entries[iEntry][0] - gMouse.xRel);
      if (diff < diffClosest) {
         iClosest = iEntry;
         diffClosest = diff;
      }
   }
   var canv = document.getElementById("cHistory");
   var ctx = canv.getContext("2d");
   
   ctx.save();
   ctx.beginPath();
   ctx.strokeStyle = "black";
   
   ctx.font = "12px Helvetica";
   
   var xPos = gHistory.entries[iClosest][0];
   
   //Start and End points of vertical line
   var p0 = ToCanvasCoords(xPos, 0);
   var p1 = ToCanvasCoords(xPos, 1);
   ctx.moveTo(p0.x, p0.y);
   ctx.lineTo(p1.x, p1.y);
   
   ctx.stroke();
   
   var entry = gHistory.entries[iClosest];
   
   //Data point positions
   var pValve   = ToCanvasCoords(xPos, entry[1]);
   var pAirT    = ToCanvasCoords(xPos, entry[2]);
   var pAirReqT = ToCanvasCoords(xPos, entry[3]);
   var pHutT    = ToCanvasCoords(xPos, entry[4]);
   var pHutReqT = ToCanvasCoords(xPos, entry[5]);
   var pLM35    = ToCanvasCoords(xPos, entry[6]);
   
   ctx.fillStyle = "Wheat";
   
   //time label
   var labelText1, labelText2;
   var labelWidth;
   //time label
   labelText1 = formatTime(toAbsValue(xPos, 0), "hh:mm:ss");
   labelText2 = formatTime(toAbsValue(xPos, 0), "DD.MM.YYYY");
   labelWidth = 1.1 * Math.max(
                               ctx.measureText(labelText1).width,
                               ctx.measureText(labelText2).width
                               );
   ctx.fillRect(p0.x - 0.5 * labelWidth, p0.y + 0.01 * canv.height - 2, labelWidth, 30);
   ctx.strokeRect(p0.x - 0.5 * labelWidth, p0.y + 0.01 * canv.height - 2, labelWidth, 30);
   ctx.textAlign = "center";
   ctx.textBaseline = "top";
   ctx.fillStyle = "black";
   ctx.fillText(labelText1, p0.x, p0.y + 0.01 * canv.height);
   ctx.fillText(labelText2, p0.x, p0.y + 0.01 * canv.height + 15);
   
   // other labels
   var labs = [[pValve.y, pValve.y, "Valve " + (entry[1] * 100).toPrecision(4)],
               [pAirT.y, pAirT.y, "Air Temp " + toAbsValue(entry[2], 1).toPrecision(4) + "°C"],
               [pAirReqT.y, pAirReqT.y, "Target Air Temp " + toAbsValue(entry[3], 1).toPrecision(4) + "°C"],
               [pHutT.y, pHutT.y, "Hut Temp " + toAbsValue(entry[4], 1).toPrecision(4) + "°C"],
               [pHutReqT.y, pHutReqT.y, "Target Hut Temp " + toAbsValue(entry[5], 1).toPrecision(4) + "°C"],
               [pLM35.y, pLM35.y, "LM35 Temp " + toAbsValue(entry[6], 1).toPrecision(4) + "°C"]];
   
   var isArranged = 0;
   var iterations = 0;
   var sep = 17;
   var maxHeight = 0.9 * canv.height - 0.5 * sep;
   var minHeight = 0.1 * canv.height + 0.5 * sep;
   while (!isArranged && ++iterations < 50) {
      isArranged = 1;
      for (var i = 0; i < 6; ++i) {
         if (labs[i][0] > maxHeight) {
            labs[i][0] = maxHeight;
            isArranged = 0;
         } else if (labs[i][0] < minHeight) {
            labs[i][0] = minHeight;
            isArranged = 0;
         }
         if (i > 0) {
            var d = labs[i][0] - labs[i - 1][0];
            if (labs[i][1] < labs[i - 1][1]) {
               //swap them
               var x = labs[i];
               labs[i] = labs[i-1];
               labs[i-1] = x;
               isArranged = 0;
               d = -d;
            }
            if (d < sep) {
               //separation needs to be increased
               if (labs[i][0] > maxHeight - 0.5 * sep) {
                  labs[i][0] = maxHeight;
                  labs[i-1][0] = maxHeight - sep;
               } else if (labs[i-1][0] < minHeight + 0.5 * sep) {
                  labs[i][0] = minHeight + sep;
                  labs[i-1][0] = minHeight;
               } else {
                  labs[i][0] += 0.5 * (sep - d);
                  labs[i-1][0] -= 0.5 * (sep - d);
               }
               isArranged = 0;
            }
         }
      }
   }
   
   //draw it
   labelWidth = 1.06 * Math.max(
                                ctx.measureText(labs[0][2]).width,
                                ctx.measureText(labs[1][2]).width,
                                ctx.measureText(labs[2][2]).width,
                                ctx.measureText(labs[3][2]).width,
                                ctx.measureText(labs[4][2]).width,
                                ctx.measureText(labs[5][2]).width
                                );
   
   for (var i = 0; i < 6; ++i) {
      ctx.textBaseline = "middle";
      ctx.textAlign = "left";
      ctx.fillStyle = "Wheat";
      ctx.fillRect(p0.x,
                   labs[i][0] - 8,
                   labelWidth,
                   15);
      ctx.strokeRect(p0.x,
                     labs[i][0] - 8,
                     labelWidth,
                     15);
      ctx.fillStyle = "Black";
      ctx.fillText(labs[i][2],
                   p0.x + 0.03 * labelWidth,
                   labs[i][0]);
   }
   ctx.stroke();
   ctx.restore();
}


function drawMouseSelection() {
   var canv = document.getElementById("cHistory");
   var ctx = canv.getContext("2d");
   
   ctx.save();
   
   //Start and End points of vertical line
   var p0 = ToCanvasCoords(0, 0);
   var p1 = ToCanvasCoords(1, 1);
   
   ctx.fillStyle = "rgba(0, 0, 255, 0.2)";
   
   ctx.fillRect(Math.min(gMouse.xAbsPush, gMouse.xAbs), p1.y, Math.abs(gMouse.xAbsPush - gMouse.xAbs), p0.y - p1.y);
   ctx.stroke();
   
   ctx.restore();
   
}


function resizeCanvas(fullScreen) {
   var canv = document.getElementById("cHistory");
   if (fullScreen) {
      canv.width = 0.98 * window.innerWidth;
      canv.height = 0.98 * window.innerHeight - 60;
      var back = (new URL(window.location.href)).searchParams.get("back");
      gLinkBox.linksTo = back ? back : "index.html";
      gLinkBox.maximise = false;
   } else {
      var col = document.getElementById("histCol");
      canv.width = 0.98 * col.offsetWidth;
      canv.height = 0.98 * window.innerHeight - col.getBoundingClientRect().top - 100;
      if (canv.height < 450) canv.height = 450;
      if (window.location.pathname == "/expert.html") {
         gLinkBox.linksTo = "History.html?back=expert.html";
      } else if (window.location.pathname == "/debug.html") {
         gLinkBox.linksTo = "History.html?back=debug.html";
      } else if (window.location.pathname == "/user.html") {
         gLinkBox.linksTo = "History.html?back=user.html";
      } else {
         gLinkBox.linksTo = "History.html";
      }
      gLinkBox.maximise = true;
   }
   gLinkBox.width = 30;
   gLinkBox.height = 30;
   gLinkBox.xpos = canv.width - gLinkBox.width;
   gLinkBox.ypos = 0.1 * canv.height;
   drawHistory();
}

function ToCanvasCoords(x, y) {
   var out = {};
   var canv = document.getElementById("cHistory");
   out.x = 0.8 * canv.width * x + 0.1 * canv.width;
   out.y = 0.8 * canv.height * (1 - y) + 0.1 * canv.height;
   return out;
}

function formatTime(t, f) {
   //t in sec, format f like "mm:ss" or "YY MM DD, hh:mm:ss"
   var res;
   var date = new Date(t * 1000);
   var sec = date.getSeconds();
   var min = date.getMinutes();
   var h = date.getHours();
   var day = date.getDate();
   var M = date.getMonth() + 1;
   var YYYY = date.getFullYear();
   var YY = YYYY % 100;
   
   res = f.replace("ss", (sec < 10) ? "0" + sec : sec);
   res = res.replace("mm", (min < 10) ? "0" + min : min);
   res = res.replace("hh", (h < 10) ? "0" + h : h);
   res = res.replace("DD", (day < 10) ? "0" + day : day);
   res = res.replace("MM", (M < 10) ? "0" + M : M);
   res = res.replace("YYYY", YYYY);
   res = res.replace("YY", YY);
   return res;
}

function drawCoordSys(xmin, ymin, xmax, ymax) {
   
   var canv = document.getElementById("cHistory");
   const coordSys = canv.getContext('2d');
   
   // clean it
   coordSys.clearRect(0,0, canv.width, canv.height);
   coordSys.clearRect(0,0, coordSys.width, coordSys.height);
   coordSys.beginPath();
   
   
   coordSys.strokeStyle = "black"
   gHistory.fontSize = Math.max(12, Math.floor(Math.min(canv.height / 30, canv.width / 60)));
   coordSys.font = gHistory.fontSize + "px Helvetica";
   
   // draw a box
   var xymin = ToCanvasCoords(0, 0);
   var xymax = ToCanvasCoords(1, 1);
   coordSys.strokeRect(xymin.x, xymax.y, xymax.x - xymin.x, xymin.y - xymax.y);
   
   // Add some labels
   //time-axis
   coordSys.textAlign = "center";
   coordSys.textBaseline = "top";
   var tLabSpacing = [2, 10, 30, 60,
                      2 * 60, 5 * 60, 10 * 60, 30*60,
                      3600, 2 * 3600, 6 * 3600,
                      12 * 3600, 24 * 3600];
   var tLabFormat  = ["hh:mm:ss", "hh:mm:ss", "hh:mm:ss", "hh:mm",
                      "hh:mm", "hh:mm", "hh:mm", "hh:mm",
                      "hhh", "hhh", "DD.MM hhh",
                      "DD.MM hhh", "DD.MM"];
   
   var nDiv = Math.min(12, 8e-3 * canv.width);
   var labInc = (xmax - xmin) / nDiv;
   var i = 0;
   while (labInc > tLabSpacing[i]) ++i;
   labInc = tLabSpacing[i];
   var labFormat = tLabFormat[i];
   var d = new Date();
   var tzOffset = 60 *  d.getTimezoneOffset();
   var labXmin = Math.ceil((xmin - tzOffset) / labInc) * labInc + tzOffset;
   var labXmax = Math.floor((xmax - tzOffset) / labInc) * labInc + tzOffset;
   nDiv = Math.floor((labXmax - labXmin) / labInc);
   
   var labY = xymin.y + 0.01 * canv.height;
   var labX = xymin.x +  (xymax.x - xymin.x) * (labXmin - xmin) / (xmax - xmin);
   var labXinc = (xymax.x - xymin.x) * (labXmax - labXmin) / (xmax - xmin) / (nDiv);
   var lab = labXmin;
   coordSys.fillText(formatTime(xmin, "DD.MM.YYYY"), xymin.x, labY + 1.2 * gHistory.fontSize);
   for (var i = 0; i < nDiv + 1; ++i) {
      coordSys.moveTo(labX, xymin.y);
      coordSys.lineTo(labX, xymin.y - 0.01 * canv.height);
      coordSys.fillText(formatTime(lab, labFormat), labX, labY)
      lab += labInc;
      labX += labXinc;
   }
   coordSys.fillText(formatTime(xmax, "DD.MM.YYYY"), xymax.x, labY + 1.2 * gHistory.fontSize);
   
   //y-axis
   coordSys.textAlign = "right";
   coordSys.textBaseline = "middle";
   var yLabSpacing = [0.1, 0.2, 0.5, 1, 2, 5, 10, 20];
   labInc = (ymax - ymin) / (12);
   i = 0;
   while (labInc > yLabSpacing[i]) ++i;
   labInc = yLabSpacing[i];
   var labYmin = Math.ceil(ymin / labInc) * labInc;
   var labYmax = Math.floor(ymax / labInc) * labInc;
   nDiv = Math.round((labYmax - labYmin) / labInc);
   
   labY = xymin.y +  (xymax.y - xymin.y) * (labYmin - ymin) / (ymax - ymin);
   labX = xymin.x - 0.01 * canv.width;
   var labYinc = (xymax.y - xymin.y) * (labYmax - labYmin) / (ymax - ymin) / (nDiv);
   lab = labYmin;
   for (var i = 0; i < nDiv + 1; ++i) {
      coordSys.moveTo(xymin.x, labY);
      coordSys.lineTo(xymin.x + 0.01 * canv.width, labY);
      coordSys.fillText(lab.toPrecision(3), labX, labY)
      lab += labInc;
      labY += labYinc;
   }
   
   //valve-axis
   coordSys.textAlign = "left";
   coordSys.textBaseline = "middle";
   labX = xymax.x + 0.01 * canv.width;
   labY = xymin.y;
   nDiv = 10;
   labYinc = (xymax.y - xymin.y) / (nDiv);
   lab = 0;
   labInc = 100 / (nDiv) ;
   for (var i = 0; i < nDiv + 1; ++i) {
      coordSys.moveTo(xymax.x, labY);
      coordSys.lineTo(xymax.x - 0.01 * canv.width, labY);
      coordSys.fillText(lab, labX, labY)
      lab += labInc;
      labY += labYinc;
   }
   coordSys.stroke();
}

function drawHistory() {
   var canv = document.getElementById("cHistory");
   const cGraph = canv.getContext("2d");
   drawCoordSys(gHistory.minTime, gHistory.minTemp, gHistory.maxTime, gHistory.maxTemp);
   
   
   var nEntries = 6;
   var legY = 0.065 * canv.height;
   var legX = 0.1 * canv.width;
   var legXinc = (0.8 * canv.width - cGraph.measureText("Air Temp LM35 Temp Hut Temp Target Air Temp Target Hut Temp Valve (right axis)").width) / (nEntries - 0.5);
   if (canv.width < 625) {
      //Two lines legend
      nEntries = 3;
      legY = 0.06 * canv.height;
      legXinc = (0.8 * canv.width - cGraph.measureText("Target Air Temp Target Hut Temp Valve (right axis)").width) / (nEntries - 0.5);
   }
   var legLineStart = 0.05 * legXinc;
   var legLineEnd = 0.4 * legXinc;
   var legTextStart = 0.5 * legXinc;
   
   cGraph.textBaseline = "middle";
   cGraph.textAlign = "left";
   
   //time threshold to draw solid line between two points:
   //note that times are between 0 and 1
   var tThr = 2.5 / gHistory.nEntries;
   
   // Air Temperature (built in)
   // Graph
   if (gHistory.entries.length) {
      cGraph.beginPath();
      cGraph.strokeStyle = "gray"
      var p = ToCanvasCoords(gHistory.entries[0][0], gHistory.entries[0][2]);
      cGraph.moveTo(p.x, p.y);
      for (var i = 1; i < gHistory.nEntries; ++i) {
         p = ToCanvasCoords(gHistory.entries[i][0], gHistory.entries[i][2]);
         if (Math.abs(gHistory.entries[i][0] - gHistory.entries[i-1][0]) > tThr) {
            cGraph.moveTo(p.x, p.y);
         } else {
            cGraph.lineTo(p.x, p.y);
         }
      }
      cGraph.stroke();
   }
   // Legend
   cGraph.fillStyle = "gray";
   cGraph.fillRect(legX + legLineStart, legY - 1, legLineEnd, 3);
   cGraph.fillStyle = "black";
   cGraph.stroke();
   cGraph.fillText("Air Temp", legX + legTextStart, legY);
   legX += legXinc + cGraph.measureText("Air Temp").width;
   
   // LM35 Temperature
   // Graph
   if (gHistory.entries.length) {
      cGraph.beginPath();
      cGraph.strokeStyle = "blue"
      var p = ToCanvasCoords(gHistory.entries[0][0], gHistory.entries[0][6]);
      cGraph.moveTo(p.x, p.y);
      for (var i = 1; i < gHistory.nEntries; ++i) {
         p = ToCanvasCoords(gHistory.entries[i][0], gHistory.entries[i][6]);
         if (Math.abs(gHistory.entries[i][0] - gHistory.entries[i-1][0]) > tThr) {
            cGraph.moveTo(p.x, p.y);
         } else {
            cGraph.lineTo(p.x, p.y);
         }
      }
      cGraph.stroke();
   }
   // Legend
   cGraph.fillStyle = "blue";
   cGraph.fillRect(legX + legLineStart, legY - 1, legLineEnd, 3);
   cGraph.fillStyle = "black";
   cGraph.stroke();
   cGraph.fillText("LM35 Temp", legX + legTextStart, legY);
   legX += legXinc + cGraph.measureText("LM35 Temp").width;
   
   //Hut Temperature
   
   if (gHistory.entries.length) {
      cGraph.beginPath();
      cGraph.strokeStyle = "darkred"
      p = ToCanvasCoords(gHistory.entries[0][0], gHistory.entries[0][4]);
      cGraph.moveTo(p.x, p.y);
      for (var i = 1; i < gHistory.nEntries; ++i) {
         p = ToCanvasCoords(gHistory.entries[i][0], gHistory.entries[i][4]);
         if (Math.abs(gHistory.entries[i][0] - gHistory.entries[i-1][0]) > tThr) {
            cGraph.moveTo(p.x, p.y);
         } else {
            cGraph.lineTo(p.x, p.y);
         }
      }
      cGraph.stroke();
   }
   // Legend
   cGraph.fillStyle = "darkred";
   cGraph.fillRect(legX + legLineStart, legY - 1, legLineEnd, 3);
   cGraph.fillStyle = "black";
   cGraph.stroke();
   cGraph.fillText("Hut Temp", legX + legTextStart, legY);
   legX += legXinc + cGraph.measureText("Hut Temp").width;
   
   if (nEntries == 3) {
      //line break for two line legend
      legY = 0.085 * canv.height;
      legX = 0.1 * canv.width;
   }
   
   //Target Air Temperature
   
   if (gHistory.entries.length) {
      cGraph.beginPath();
      cGraph.strokeStyle = "Green"
      p = ToCanvasCoords(gHistory.entries[0][0], gHistory.entries[0][3]);
      cGraph.moveTo(p.x, p.y);
      for (var i = 1; i < gHistory.nEntries; ++i) {
         p = ToCanvasCoords(gHistory.entries[i][0], gHistory.entries[i][3]);
         if (Math.abs(gHistory.entries[i][0] - gHistory.entries[i-1][0]) > tThr) {
            cGraph.moveTo(p.x, p.y);
         } else {
            cGraph.lineTo(p.x, p.y);
         }
      }
      cGraph.stroke();
   }
   // Legend
   cGraph.fillStyle = "Green";
   cGraph.fillRect(legX + legLineStart, legY - 1, legLineEnd, 3);
   cGraph.fillStyle = "black";
   cGraph.stroke();
   cGraph.fillText("Target Air Temp", legX + legTextStart, legY);
   legX += legXinc + cGraph.measureText("Target Air Temp").width;
   
   
   if (gHistory.entries.length) {
      cGraph.beginPath();
      cGraph.strokeStyle = "Fuchsia"
      p = ToCanvasCoords(gHistory.entries[0][0], gHistory.entries[0][5]);
      cGraph.moveTo(p.x, p.y);
      for (var i = 1; i < gHistory.nEntries; ++i) {
         p = ToCanvasCoords(gHistory.entries[i][0], gHistory.entries[i][5]);
         if (Math.abs(gHistory.entries[i][0] - gHistory.entries[i-1][0]) > tThr) {
            cGraph.moveTo(p.x, p.y);
         } else {
            cGraph.lineTo(p.x, p.y);
         }
      }
      cGraph.stroke();
   }
   // Legend
   cGraph.fillStyle = "Fuchsia";
   cGraph.fillRect(legX + legLineStart, legY - 1, legLineEnd, 3);
   cGraph.fillStyle = "black";
   cGraph.stroke();
   cGraph.fillText("Target Hut Temp", legX + legTextStart, legY);
   legX += legXinc + cGraph.measureText("Target Hut Temp").width;
   
   
   if (gHistory.entries.length) {
      cGraph.beginPath();
      cGraph.strokeStyle = "black"
      p = ToCanvasCoords(gHistory.entries[0][0], gHistory.entries[0][1]);
      cGraph.moveTo(p.x, p.y);
      for (var i = 1; i < gHistory.nEntries; ++i) {
         p = ToCanvasCoords(gHistory.entries[i][0], gHistory.entries[i][1]);
         if (Math.abs(gHistory.entries[i][0] - gHistory.entries[i-1][0]) > tThr) {
            cGraph.moveTo(p.x, p.y);
         } else {
            cGraph.lineTo(p.x, p.y);
         }
      }
      cGraph.stroke();
   }
   // Legend
   cGraph.fillStyle = "black";
   cGraph.fillRect(legX + legLineStart, legY - 1, legLineEnd, 3);
   cGraph.fillStyle = "black";
   cGraph.stroke();
   cGraph.fillText("Valve (right axis)", legX + legTextStart, legY);
   
   if (gMouse.isOverActiveArea) {
      drawMouseOver();
   }
   
   if (gMouse.isPushed) {
      drawMouseSelection();
   }
   
   drawLinkBox();
}

function rpc_gethistory()
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         
         gHistory.entries = o.history;
         gHistory.minTime = o.minTime;
         gHistory.maxTime = o.maxTime;
         gHistory.minTemp = o.minTemp;
         gHistory.maxTemp = o.maxTemp;
         gHistory.nEntries = o.nEntries;
         
         drawHistory();
         
         setTimeout(rpc_gethistory, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "getHistory";
   request.params = [];
   var radios = document.getElementsByName("time");
   var time = 0;
   var nEntries = 0;
   for (var i = 0; i < radios.length; ++i) {
      if (radios[i].checked) {
         gHistory.timeSpan = radios[i].value;
      }
   };
   var d = new Date();
   d.setTime(d.getTime() + 3000);
   document.cookie = "timeSpan=" + gHistory.timeSpan + ";expires=" + d.toGMTString() + ";path=/;samesite=lax";
   if (gHistory.timeSpan == -1) {
      //only available in debug mode
      time = 120;
      nEntries = 12;
   } else {
      time = gHistory.timeSpan;
      nEntries = Math.min(document.getElementById("cHistory").width, time, 1000);
   }
   request.params[0] = nEntries;
   request.params[1] = time;
   request.params[2] = 0;
   
   request.id = 1;
   
   if (gHistory.timeSpan == -2 || gHistory.isPaused) {
      //paused debug mode or zoomed mode
      setTimeout(rpc_gethistory, 1000);
   } else {
      xhttp.open("POST", "/json-rpc", true);
      xhttp.setRequestHeader("Content-type", "application/json");
      xhttp.send(JSON.stringify(request));
   }
}

function rpc_requestzoom(t1, t2) {
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         
         gHistory.entries = o.history;
         gHistory.minTime = o.minTime;
         gHistory.maxTime = o.maxTime;
         gHistory.minTemp = o.minTemp;
         gHistory.maxTemp = o.maxTemp;
         gHistory.nEntries = o.nEntries;
         
         drawHistory();
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "getHistory";
   request.params = [];
   
   var canv = document.getElementById("cHistory");
   var nEntries = Math.min(0.8 * canv.width, Math.abs(t1 - t2), 1000);
   
   
   var d = new Date();
   d.setTime(d.getTime() + 3600 * 1000);
   document.cookie = "t1=" + t1 + ";expires=" + d.toGMTString() + ";path=/";
   document.cookie = "t2=" + t2 + ";expires=" + d.toGMTString() + ";path=/";
   
   if (t1 == t2) {
      // unzoom and bau
      gHistory.isPaused = false;
      rpc_gethistory();
      
   } else {
      gHistory.isPaused = true;
      request.params[0] = nEntries;
      request.params[1] = Math.min(t1, t2);
      request.params[2] = Math.max(t1, t2);
      
      request.id = 1;
      xhttp.open("POST", "/json-rpc", true);
      xhttp.setRequestHeader("Content-type", "application/json");
      xhttp.send(JSON.stringify(request));
   }
}
