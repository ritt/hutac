function rpc_readtemp()
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         document.getElementById("tempAir").innerHTML = o.result[0];
         document.getElementById("tempHut").innerHTML = o.result[1];
         document.getElementById("tempLM35").innerHTML = o.result[2];
         setTimeout(rpc_readtemp, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "readtemp";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_readValve() {
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         document.getElementById("valveStatus").innerHTML = o.valve;
         setTimeout(rpc_readValve, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "readValve";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_readChannels()
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         if (o.result[0] == 0) {
            document.getElementById("AirRead").innerHTML = "built in";
            document.getElementById("ButAirChannel").value = "To LM35";
            document.getElementById("ButAirChannel").onclick = function() {rpc_setChannel(0, 2)};
         } else if (o.result[0] == 2) {
            document.getElementById("AirRead").innerHTML = "LM35";
            document.getElementById("ButAirChannel").value = "To built in";
            document.getElementById("ButAirChannel").onclick = function() {rpc_setChannel(0, 0)};
         }
         setTimeout(rpc_readChannels, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "getChannels";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setChannel(control, channel)
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setChannel";
   request.params = [];
   request.params[0] = control;
   request.params[1] = channel;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_readPID(mode)
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         if (o.airActive) {
            document.getElementById("AirStatus").innerHTML ="Active";
            document.getElementById("ValveControl").innerHTML="Set by Air control";
            document.getElementById("valve").style.display = "none";
            document.getElementById("setValve").style.display = "none";
            document.getElementById("ButAirActive").value = "Air Off";
            document.getElementById("ButAirActive").onclick = function() {rpc_setactive(0, 0)};
         } else {
            document.getElementById("AirStatus").innerHTML ="inactive";
            document.getElementById("ValveControl").innerHTML="control manually";
            document.getElementById("valve").style.display = "initial";
            document.getElementById("setValve").style.display = "initial";
            document.getElementById("ButAirActive").value = "Air On";
            document.getElementById("ButAirActive").onclick = function() {rpc_setactive(0, 1)};
         }
         if (o.hutActive) {
            document.getElementById("HutStatus").innerHTML ="Active";
            document.getElementById("ButHutActive").value = "Hut Off";
            document.getElementById("SetAirTemp").style.display = "none";
            document.getElementById("ButSetAirTemp").style.display = "none";
            document.getElementById("ButHutActive").onclick = function() {rpc_setactive(1, 0)};
         } else {
            document.getElementById("HutStatus").innerHTML ="inactive";
            document.getElementById("ButHutActive").value = "Hut On";
            document.getElementById("SetAirTemp").style.display = "initial";
            document.getElementById("ButSetAirTemp").style.display = "initial";
            document.getElementById("ButHutActive").onclick = function() {rpc_setactive(1, 1)};
         }
         
         if (mode == 1) {
            document.getElementById("SetAirP").value = o.airPID[0];
            document.getElementById("SetAirI").value = o.airPID[1];
            document.getElementById("SetAirD").value = o.airPID[2];
            document.getElementById("SetHutP").value = o.hutPID[0];
            document.getElementById("SetHutI").value = o.hutPID[1];
            document.getElementById("SetHutD").value = o.hutPID[2];
         }
         
         document.getElementById("GetAirP").innerHTML = o.airPID[0];
         document.getElementById("GetAirI").innerHTML = o.airPID[1];
         document.getElementById("GetAirD").innerHTML = o.airPID[2];
         document.getElementById("GetAirReqT").innerHTML = o.airReqTemp;
         document.getElementById("GetHutP").innerHTML = o.hutPID[0];
         document.getElementById("GetHutI").innerHTML = o.hutPID[1];
         document.getElementById("GetHutD").innerHTML = o.hutPID[2];
         document.getElementById("GetHutReqT").innerHTML = o.hutReqTemp;
         
         setTimeout(function() {rpc_readPID(0);}, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "readpid";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setvalve()
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         //document.getElementById("result").innerHTML = "Result: "+o.result;
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setvalve";
   request.params = [];
   request.params[0] = document.getElementById("valve").value;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setTemp(control)
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         //document.getElementById("result").innerHTML = "Result: "+o.result;
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setTemp";
   request.params = [];
   request.params[0] = control;
   if (control == 0) {
      request.params[1] = document.getElementById("SetAirTemp").value;
   } else {
      request.params[1] = document.getElementById("SetHutTemp").value;
   }
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setpid(mode = 0)
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         //document.getElementById("result").innerHTML = "Result: "+o.result;
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setpid";
   request.params = [];
   request.params[0] = mode;
   if (mode == 0) {
      //The main button was pushed - set all values
      request.params[1] = document.getElementById("SetAirP").value;
      request.params[2] = document.getElementById("SetAirI").value;
      request.params[3] = document.getElementById("SetAirD").value;
      request.params[4] = document.getElementById("SetHutP").value;
      request.params[5] = document.getElementById("SetHutI").value;
      request.params[6] = document.getElementById("SetHutD").value;
   } else if (mode == 1) {
      //Air Prop value
      request.params[1] = document.getElementById("SetAirP").value;
   } else if (mode == 2) {
      //Air Int value
      request.params[1] = document.getElementById("SetAirI").value;
   } else if (mode == 3) {
      //Air Diff value
      request.params[1] = document.getElementById("SetAirD").value;
   } else if (mode == 4) {
      //Hut Prop value
      request.params[1] = document.getElementById("SetHutP").value;
   } else if (mode == 5) {
      //Hut Int value
      request.params[1] = document.getElementById("SetHutI").value;
   } else if (mode == 6) {
      //Hut Diff value
      request.params[1] = document.getElementById("SetHutD").value;
   }
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setactive(control, status)
{
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setActive";
   request.params = [];
   request.params[0] = control;
   request.params[1] = status;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}


function rpc_setTS(index)
{
   var xhttp = new XMLHttpRequest();

   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setStatTime";
   request.params = [];
   request.params[0] = index;
   request.params[1] = document.getElementById("SetTS"+index).value ;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setUL(index)
{
   var xhttp = new XMLHttpRequest();

   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setStatUp";
   request.params = [];
   request.params[0] = index;
   request.params[1] = document.getElementById("SetUL"+index).value ;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_setLL()
{
   var xhttp = new XMLHttpRequest();

   var request = {};
   request.jsonrpc = "2.0";
   request.method = "setStatLow";
   request.params = [];
   request.params[0] = document.getElementById("SetLL").value ;
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_readStatus() {
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         document.getElementById("AlarmStatus").innerHTML = o.status;
         for (var i = 0; i < 4; ++i) {
            if (i == o.index) {
               document.getElementById("TS" + i).style.background = "red";
               document.getElementById("UL" + i).style.background = "red";
            } else {
               document.getElementById("TS" + i).style.background = document.body.style.background;
               document.getElementById("UL" + i).style.background = document.body.style.background;
            }
         }
         setTimeout(rpc_readStatus, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "getStatus";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}

function rpc_readStatusPars(mode) {
   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         o = JSON.parse(xhttp.responseText);
         document.getElementById("LowIntLimit").innerHTML = o.low;
         for (var i = 0; i < o.span.length; ++i) {
            document.getElementById("TS" + i).innerHTML = o.span[i];
            document.getElementById("UL" + i).innerHTML = o.up[i];
            if (mode == 1) {
               document.getElementById("SetTS"+i).value = o.span[i];
               document.getElementById("SetUL"+i).value = o.up[i];
            }
         }

         setTimeout(function() {rpc_readStatusPars(0);}, 1000);
      }
   };
   
   var request = {};
   request.jsonrpc = "2.0";
   request.method = "getStatPar";
   request.id = 1;
   
   xhttp.open("POST", "/json-rpc", true);
   xhttp.setRequestHeader("Content-type", "application/json");
   xhttp.send(JSON.stringify(request));
}
