#!/bin/bash

cd "$(dirname "$0")/bin"
if [ -f main.log ]; then
	rm main.log
fi

./main > main.log &

echo "Starting main PID control process"

# wait until the main routine enters running mode
# this ensures that all shared objects are properly initialised
sleep 1
while [ `grep 'initialisation completed, entering running mode' main.log |wc -l` -eq 0 ]
do
	sleep 1
done

echo "PID control process initialised. Starting servers ..."

./server > server.log &
sleep 2
echo "control server started"
./udpSocket > udp.log &
sleep 2
echo "UDP socket started"

exit 0
