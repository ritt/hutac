//
//  historyEntry.h
//  
//
//  Created by Patrick Schwendimann on 12.12.18.
//

#ifndef historyEntry_h
#define historyEntry_h

struct historyEntry {
   long fTime;
   double fValve;
   double fAirTemp;
   double fAirReqTemp;
   double fHutTemp;
   double fHutReqTemp;
   double fLM35Temp;
};

historyEntry operator+= (historyEntry&, historyEntry);
historyEntry operator/ (historyEntry, int);
historyEntry operator*= (historyEntry&, double);

#define NUM_HIST_STAT 4
struct historyStatusParameters {
   float low = 0.1;
   float timespan[NUM_HIST_STAT] = {0.};
   float upLim[NUM_HIST_STAT] = {0.};
};
#endif /* historyEntry_h */
