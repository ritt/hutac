//
//  historyReader.h
//  
//
//  Created by Patrick Schwendimann on 13.05.2019
//

#ifndef historyreader_h
#define historyreader_h

#include <vector>
#include "historyEntry.h"
class sharedMemory;

class HistoryReader {
public:
   HistoryReader(sharedMemory*);
   ~HistoryReader();
   
   std::vector<historyEntry> GetHistory(long, long, long);
   std::vector<historyEntry> GetHistory(long, long);
   void Dump(long, long, bool);
   
   historyEntry *GetLastEntry();
   float GetOffsetStatus(int* id = 0);
   
private:
   historyEntry *fHistory;
   historyStatusParameters *fStatPar;
   long *fHistorySize;
   long *fCurrentIndex;
   bool *fCycleCompleted;
};
#endif /* historyreader_h */
