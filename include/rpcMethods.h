#ifndef rpcMethods_H
#define rpcMethods_H

#include "mongoose.h"

class sharedMemory;
class HistoryReader;
struct pidPars;

class RPC {
public:
   RPC(sharedMemory*);
   ~RPC();
   
   static void ev_handler(struct mg_connection *nc, int ev, void *ev_data);
   
   static int GetTemp(char*, int , struct mg_rpc_request*);
   static int GetPID(char*, int, struct mg_rpc_request*);
   static int GetValve(char*, int, struct mg_rpc_request*);
   static int GetHistory(char*, int, struct mg_rpc_request*);
   
   static int SetActive(char*, int, struct mg_rpc_request*);
   static int SetAllActive(char*, int, struct mg_rpc_request*);
   static int SetTemp(char*, int , struct mg_rpc_request*);
   
   // For Experts
   
   static int SetChannel(char*, int, struct mg_rpc_request*);
   static int GetChannels(char*, int, struct mg_rpc_request*);
   
   static int SetPID(char*, int, struct mg_rpc_request*);
   static int SetValve(char*, int, struct mg_rpc_request*);
   static int DumpHistory(char*, int, struct mg_rpc_request*);
   static int SetDebug(char*, int, struct mg_rpc_request*);
   static int GetDebug(char*, int, struct mg_rpc_request*);

   static int SetStatusLow(char*, int, struct mg_rpc_request*);
   static int SetStatusTime(char*, int, struct mg_rpc_request*);
   static int SetStatusUp(char*, int, struct mg_rpc_request*);
   static int GetStatusParameters(char*, int, struct mg_rpc_request*);
   static int GetStatus(char*, int, struct mg_rpc_request*);
   
   inline void SetHistory(HistoryReader* hist) {fHistory = hist; };
   
   inline void SetServerOpts(struct mg_serve_http_opts opts) {s_http_server_opts = opts;}
   
   //For curl readout
   
   static void curlHandler(struct mg_connection*, int, void*);
   
   
private:
   
   static struct mg_serve_http_opts s_http_server_opts;
   
   static sharedMemory* fShared;
   static HistoryReader* fHistory;
   static pidPars* fPID0;
   static pidPars* fPID1;
   
};

#endif
