#ifndef hardwareConnection_H
#define hardwareConnection_H
#include "control.h"

class HWConnection : public control {
private:
   double fValve;
   double fTemperature[4];
   double *fTemperatureArray[4];
   unsigned int fArraySize;
   unsigned int fIndex;
   unsigned int fCycles;
   unsigned int fIntegrationWindow;
   double fConversionFactor[4];
#ifdef __ARMEL__
   int fspi_fd0, fspi_fd1;
#endif
   
public:
   HWConnection();
   ~HWConnection();
   
   inline unsigned int GetArraySize() {return fArraySize;};
   
   inline void SetCycles(unsigned int val) {fCycles = val; };
   inline unsigned int GetCycles() {return fCycles; };
   inline void SetIntegrationWindow(unsigned int val) {fIntegrationWindow = val; };
   inline unsigned int GetIntegrationWindow() {return fIntegrationWindow; };

   void ReadTemperatures(unsigned int);
   void ReadTemperatures();
   void SetValve(double);
   double GetValve();
   double GetTemperature(unsigned int);
   
   virtual void SetControlledValue(double);
};


#endif
