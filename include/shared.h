#ifndef SHARED_h
#define SHARED_h

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

struct historyStatusParameters;
struct historyEntry;
struct pidPars;

class sharedMemory {
public:
   sharedMemory(bool);
   ~sharedMemory();
   
   
   inline void GetDebug(int* addr) {addr = fDebug;};
   inline int GetDebug() {return *fDebug;};
   inline void SetDebug(int val) {*fDebug = val; };
   pidPars *GetPIDpars(int);
   inline long* GetCurrentIndex() {return fCurrentIndex; };
   inline long* GetHistorySize() {return fHistorySize; };
   inline bool* GetCycleCompleted() {return fCycleCompleted; };
   inline historyStatusParameters* GetHistoryStatusParameters() {return fHistoryStatPars; };
   inline historyEntry* GetHistory() {return fHistory; };
   inline int GetCommand() {return *fCommand; };
   void SetCommand(int, float arg = 0);
   inline int GetArgument() {return *fArgument; };
   
private:
   boost::interprocess::shared_memory_object *fShared;
   boost::interprocess::mapped_region *fReg;
   bool fIsMaster;
   int *fDebug;
   int *fCommand;
   float *fArgument;
   pidPars *fPID0;
   pidPars *fPID1;
   long* fCurrentIndex;
   long* fHistorySize;
   bool* fCycleCompleted;
   historyStatusParameters *fHistoryStatPars;
   historyEntry *fHistory;
};

#endif
