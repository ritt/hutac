//
//  history.h
//  
//
//  Created by Patrick Schwendimann on 12.12.18.
//

#ifndef history_h
#define history_h

#include <vector>
#include "historyEntry.h"
class HWConnection;
class pidControl;
class sharedMemory;

class History {
public:
   History(sharedMemory*);
   ~History();
   
   inline void SetHardwareConnection(HWConnection* hwc) {fHWConn = hwc; };
   inline void SetAirControl(pidControl* ctrl) {fAirControl = ctrl; };
   inline void SetHutControl(pidControl* ctrl) {fHutControl = ctrl; };
   inline void SetInterval(double t) {fInterval = t; };
   
   void TimeStep();
   void Write();
   std::vector<historyEntry> GetHistory(long, long, long);
   std::vector<historyEntry> GetHistory(long, long);
   void Dump(long, long, bool);
   void ReadFromFiles();
   int WriteStatus();
   void ReadStatus(char*);
   void CleanUp();
   
   historyEntry GetLastEntry();
   
private:
   HWConnection* fHWConn;
   pidControl* fAirControl;
   pidControl* fHutControl;
   
   double fInterval;
   int fStatusInterval;
   int fCleanupInterval;
   long fLastCall;
   long fLastStatus;
   long fLastCleanup;
   
   
   historyEntry *fHistory;
   historyStatusParameters *fHistoryStatPar;
   long *fHistorySize;
   long *fCurrentIndex;
   bool *fCycleCompleted;
};
#endif /* history_h */
