#ifndef PIDcontrol_H
#define PIDcontrol_H

#include <time.h>
#include "control.h"

class control;
class HWConnection;
class sharedMemory;

struct pidPars {
   double P, I, D;
   double intDiff;
   double reqTemp;
   int channel;
   int isActive;
};

class pidControl : public control {
private:
   short fControlId;
   control* fControlled;
   HWConnection* fConnection;
   
   struct timespec fLastCallTime;
   double fLastDiff; //last measured temperature difference
   
   pidPars* fParameters;

public:
   pidControl(short, control*, HWConnection*, sharedMemory*);
   inline ~pidControl() { };
   int pidStep();
   
   virtual void SetControlledValue(double);
   
   inline void SetChannel(short val) {fParameters->channel = val; };
   inline short GetChannel() {return fParameters->channel; };
   
   inline void SetActive(bool val) {fParameters->isActive = val; };
   inline bool GetActive() {return fParameters->isActive; };
   
   inline void SetTemperature(double val) {fParameters->reqTemp = val; };
   inline void SetIntegral(double val) {fParameters->intDiff = val;}
   
   inline double GetTemperature() {return fParameters->reqTemp; };
   inline double GetIntegral() {return fParameters->intDiff; };
   
   inline void SetP(double val) {fParameters->P = val; };
   inline void SetI(double val) {fParameters->I = val; };
   inline void SetD(double val) {fParameters->D = val; };
   
   inline double GetP() {return fParameters->P; };
   inline double GetI() {return fParameters->I; };
   inline double GetD() {return fParameters->D; };
};

#endif
