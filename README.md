# hutac repository

Date: 16.06.2021  
Author: **Patrick Schwendimann**, Stefan Ritt


## Purpose

This software is used to control the temperature in the detector hut of the MEG II experiment
from a Raspberry Pi (Hostname: **megpi1**, backup: megpi3).
Apart from the PID control, it offers the possibility to access the
temperature history via network connection from a browser and to configure the settings
of the temperature control.

## Content

In total 5 different executables are contained in this repository:

* **main**:
   This includes the PID control routines as well as writing the history.
   The shared memory required to interact with the servers is initialised
   in this process. Consequently, this process has to be started first.

* **server**:
   This server will read and write to the shared memory initialised by the
   **main** process. It can be used to view the history written, get the
   current configuration or adjust any of the configurations. There is full
   read and write access to all important parts of the PID control, potentially
   turning it off or breaking its stability.

* **udpSocket**:
   This creates a UDP socket to read out the values remotely on port 3000.
   It is thought to be used as MIDAS interface.

* **adcdac**
   Contained for legacy reasons. Used to directly set analogue output voltage
   and to read analogue input voltage, mainly for basic testing purposes.
   Considered as a simple example on how to set/get input/output voltages
   on the Raspberry Pi.

* **server-ac**
   Contained for legacy reasons. Used to set the valve directly and read out
   the measured temperatures. Can be used for some intermediate testing.
   Consider it as a somewhat more advanced example on how to start a server
   and use RPC methods to interact with the ADC and DAC on the Raspberry Pi.

## Preparing the RaspberryPi
   These steps need to be followed only once when configuring
   the RaspberryPi for the first time.

* Start with basic configuration
  Default login is:
  ```
  User: pi
  Password: raspberry
  ```
  Note: it uses a QWERTY keyboard as default. If you use a german layout, the
  password you have to type on your keyboard is ```raspberrz```

  ```
  sudo raspi-config
  ```
  You want to set the hostname in this step and make sure
  that SSH is enabled. If needed, the keyboard layout needs
  to be adjusted.

  The RaspberryPi asks to restart. Login using the default account.
  As SSH is now enabled, make sure that you change the password
  of the default account. Do so by
  ```
  passwd
  ```
  and set a new password.

  Edit the file ```/boot/config.txt``` and uncomment the line
  ```
  dtparam=spi=on
  ```

  Feel free to set up a login banner that contains valuable
  information about the RaspberryPi on login. Edit the file
  ```/etc/motd``` for this purpose.

* Set up account
   ```
   sudo su
   useradd -m meg
   usermod -aG sudo meg
   usermod -aG spi meg
   usermod -aG i2c meg
   usermod -aG gpio meg
   passwd meg
   ```
   Instead of meg, any username can be chosen. The password is chosen in
   the last step and all experts should easily remember it.

* Install additional packages..
   This code requires boost libraries for the shared memory
   used for inter-process communication, CMake to create
   the makefiles and the wiringpi libraries. To transfer
   these files to your Raspberry, git is used. You can install
   it using the following command

   ```
   sudo apt-get update
   sudo apt-get install libboost-all-dev cmake wiringpi git
   ```

* Set up time protocoll
   ```
   sudo apt-get install ntp
   ```

   Depending on your local network configuration, you need to set the
   appropriate servers. For PSI, these lines should be added to the file
   ```/etc/ntp.conf```
   ```
   server pstime1
   server pstime2
   server pstime3
   ```
   Comment any other server or pool entries. They are unreachable from
   inside PSI.

   Eventually, restart the protocol and get the current time
   ```
   sudo /etc/init.d/ntp stop
   sudo ntpd -q -g
   sudo /etc/init.d/ntp start
   ```

## Compilation

To compile the code, run the following commands

```
mkdir build  
cd build  
cmake ../  
make install  
```


## Usage

After the compilation is completed, it is easiest to start everything using
the start.sh bash script. If an individual start is needed, the **main** process
containing the PID controls has to be started first. Once it is fully
initialised, the following message shows:  
*initialisation completed, entering running mode*  
The server applications should not be started before this message appears.

Should an automatic start at system startup be desired, the following line
can be added to the crontab, i.e. by typing ```crontab -e```
```
@reboot sh /PATH/TO/start.sh
```
where the appropriate absolute path to the start.sh script has to be provided.

By default, the **server**  application will start the main control
server on port 8080. The corresponding sites can be accessed from your
favourite browser under the link
```
   megpi1.psi.ch:8080     -> main server (read + write)  
```
where one potentially has to adjust the host name to the machine in use and
the port in case the default value is overridden.

The following sites are available on the server:

* **index.html**
   The default site. It offers an overview over the current status of the
   PID control and displays the recent history graphically.

* **history.html**
   Display the history in a maximised way. Offer some further time intervals 
   for selection.

* **user.html**
   Display a summary of the current status. In addition, one can set the
   target hut temperature to be kept and swich the entire PID control
   on or off. It is designed to be comprehendable without understanding
   the underlying mechanics of the PID control.

* **expert.html**
   This site allows full access to all relevant PID parameters.
   Please consider the detailed description below to understand
   the impact of those parameters.

* **debug.html**
   This site allows to print some additional information to the screen,
   which requires the processes to be attached to a terminal or the
   programme output to be redirected to a log file.

## Detailed Description

In total there are two PID control loops. The first loop is referred
to as "Air" and controls the temperature of the cold air that is blown
into the detector hut. Main purpose is to keep this temperature stable
and to counter variations that originate from the chiller itself.
It directly affects the opening of the valve in the cooling water circuit.

The second loop is referred to as "Hut" and tries to maintain a constant
temperature in the detector hut. The Build-In sensor measures the
temperature of the hot air returning from the detector hut to the
heat exchanger. This loop adjusts the requested temperature in the "Air"
loop.

Each of the loops can be switched on or off individually on the expert site.
The PID parameters for each loop can be set individually using the
"Set" button next to the entry field or one can set all six parameters at
once using the "Set PID" button below.

A potential set of starting parameters is
```
Air (P, I, D): 20.57, 0.88,    60  
Hut (P, I, D): 5    , 0.00751,  0
```
The latest values in use are stored in the files runStatus.txt (while running)
or in termStatus.txt (when exiting in an ordered way). Upon restarting,
the latest configuration will be used to initialise the control.

In addition, a status value is computed based on the average deviations
observed. This value is accessible only though the UDP socket and the
expert control panel. A value between 0 and 1 indicates a normal operation
without exceeding any thresholds. A value above 1 refers to an exceeded
threshold.

Experts can adjust these thresholds. Four individual thresholds are
implemented. The timespan refers to the time over which the average
is to be computed. The upper limit refers to the maximal acceptable
deviation.

Note that any deviation larger than three times the upper
limit are treated as three times the upper limit when computing the
average. This avoids that a short term deviation raises a long term
alarm via a long time threshold.

For each of the thresholds, the average computed as described above
is divided by the respective upper limit. The status is then given
as the maximal ratio optained in this way.

## Machine Readable Output

### curl
This requires the **server** executable to be running.
Use one of the following commands to get the current (latest) value
```
curl megpi1:8080/curl/time       -> current timestamp  
curl megpi1:8080/curl/valve      -> valve position  
curl megpi1:8080/curl/air        -> air temperature read out by built-in sensor  
curl megpi1:8080/curl/airReq     -> requested air temperature  
curl megpi1:8080/curl/hut        -> hut temperature read out by built-in sensor  
curl megpi1:8080/curl/hutReq     -> requested hut temperature  
curl megpi1:8080/curl/LM35       -> air temperature read out by LM35 sensor  
```
Certain values can be set by using one of the following commands.
```
curl megpi1:8080/curl/valve -d X  
curl megpi1:8080/curl/airReq -d X  
curl megpi1:8080/curl/hutReq -d X  
```
where X stands for the new value to set. The values for the valve and airReq are
potentially overwritten immediately by the corresponding PID control loop, if active.


### UDP
This requires the executable **udpSocket** to be running.
By default, it is listening on port 3000.
These options are available:

**READ**  
It will answer with 7 numbers, each on its own line:
```
valve  
air  
airReq  
hut  
hutReq  
LM35
status
```
where these values refer to the same values as listed above for the curl commands,
respectively to the status described above.
Note that only the current/latest values are available through machine readout.

**START**  
Start up the whole PID control. It will activate the hut and the air control loop.
After the packet is received and the PID control is activated, "done" is sent
as reply.

**STOP**  
End the PID control. It will deactivate both, the hut and the air control loop.
In addition, the valve will be completely closed.
After receiving the packet and the values are set, "done" is sent as reply.
The "done" is likely to arrive, before the valve is fully closed as the mechanical
process takes some time.

**SET** *newTemp*  
Set the requested hut tempertature to *newTemp*, where *newTemp* stands for
any string representing a double value in °C. E.g "SET 22.5" will set the
requested hut temperature to 22.5 °C.
After the new temperature is set, "done" is sent as reply.
