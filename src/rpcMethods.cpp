
#include "shared.h"
#include <vector>
#include "historyReader.h"
#include "server.h"
#include "rpcMethods.h"
#include "pidControl.h" //For pidPars struct only


HistoryReader* RPC::fHistory = 0;
pidPars* RPC::fPID0 = 0;
pidPars* RPC::fPID1 = 0;
sharedMemory* RPC::fShared = 0;
struct mg_serve_http_opts RPC::s_http_server_opts;

RPC::RPC(sharedMemory* sharedMem)
{
   fShared = sharedMem;
   fPID0 = fShared->GetPIDpars(0);
   fPID1 = fShared->GetPIDpars(1);
}

RPC::~RPC()
{
   
}

void RPC::ev_handler(struct mg_connection *nc, int ev, void *ev_data)
{
   struct http_message *hm = (struct http_message *) ev_data;
   static const char *methods[] = {
      "setvalve",
      "readValve",
      "getChannels",
      "setChannel",
      "readtemp",
      "readpid",
      "setpid",
      "setActive",
      "setAllActive",
      "setTemp",
      "dumpHistory",
      "getHistory",
      "setDebug",
      "getDebug",
      "setStatLow",
      "setStatTime",
      "setStatUp",
      "getStatPar",
      "getStatus",
      NULL
   };
   static mg_rpc_handler_t handlers[] = {
      RPC::SetValve,
      RPC::GetValve,
      RPC::GetChannels,
      RPC::SetChannel,
      RPC::GetTemp,
      RPC::GetPID,
      RPC::SetPID,
      RPC::SetActive,
      RPC::SetAllActive,
      RPC::SetTemp,
      RPC::DumpHistory,
      RPC::GetHistory,
      RPC::SetDebug,
      RPC::GetDebug,
      RPC::SetStatusLow,
      RPC::SetStatusTime,
      RPC::SetStatusUp,
      RPC::GetStatusParameters,
      RPC::GetStatus,
      NULL
   };
   char buf[100000];
   
   if (ev == MG_EV_HTTP_REQUEST) {
      if (mg_vcmp(&hm->uri, "/json-rpc") == 0) {
         mg_rpc_dispatch(hm->body.p, hm->body.len, buf, sizeof(buf),
                         methods, handlers);
         mg_printf(nc, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\n"
                   "Content-Type: application/json\r\n\r\n%s",
                   (int) strlen(buf), buf);
         nc->flags |= MG_F_SEND_AND_CLOSE;
      } else {
         // serve static content
         mg_serve_http(nc, hm, s_http_server_opts);
      }
   }
}

int RPC::GetTemp(char *buf, int len, struct mg_rpc_request *req)
{
   if (*gDEBUG) {
      printf("RPC::GetTemp called\n");
   }
   double v[4];
   historyEntry* entry = fHistory->GetLastEntry();
   v[0] = entry->fAirTemp;
   v[1] = entry->fHutTemp;
   v[2] = entry->fLM35Temp;
   v[3] = -300;
   
   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,\"result\":[", atoi(req->id->ptr));
   sprintf(buf+strlen(buf), "%1.2lf, %1.2lf, %1.2lf, %1.2lf", v[0], v[1], v[2], v[3]);
   sprintf(buf+strlen(buf), "]}\n");
   return strlen(buf);
}

int RPC::GetChannels(char *buf, int len, struct mg_rpc_request *req)
{
   double v[2];
   
   v[0] = fPID0->channel;
   v[1] = fPID1->channel;
   
   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,\"result\":[", atoi(req->id->ptr));
   sprintf(buf+strlen(buf), "%1.2lf, %1.2lf", v[0], v[1]);
   sprintf(buf+strlen(buf), "]}\n");
   return strlen(buf);
}

int RPC::SetChannel(char* buf, int len, struct mg_rpc_request *req) {
   int id = atoi(req->params[1].ptr);
   int ch = atoi(req->params[2].ptr);
   
   if (id == 0) {
      fPID0->channel = ch;
   } else if (id == 1) {
      fPID1->channel = ch;
   }
   
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::GetPID(char *buf, int len, struct mg_rpc_request *req)
{
   
   if (*gDEBUG) {
      printf("RPC::GetPID called\n");
   }
   double values[6];
   
   values[0] = fPID0->P;
   values[1] = fPID0->I;
   values[2] = fPID0->D;
   
   values[3] = fPID1->P;
   values[4] = fPID1->I;
   values[5] = fPID1->D;
   
   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d", atoi(req->id->ptr));
   sprintf(buf+strlen(buf), ",\"airPID\":[%1.3lf, %1.3lf, %1.3lf]",
           values[0], values[1], values[2]);
   sprintf(buf+strlen(buf), ",\"airActive\": %i",
           fPID0->isActive);
   sprintf(buf+strlen(buf), ",\"airReqTemp\": %1.2lf",
           fPID0->reqTemp);
   sprintf(buf+strlen(buf), ",\"hutPID\":[%1.3lf, %1.6lf, %1.3lf]",
           values[3], values[4], values[5]);
   sprintf(buf+strlen(buf), ",\"hutActive\": %i",
           fPID1->isActive);
   sprintf(buf+strlen(buf), ",\"hutReqTemp\": %1.2lf",
           fPID1->reqTemp);
   sprintf(buf+strlen(buf), "}\n");
   return strlen(buf);
}

int RPC::GetValve(char *buf, int len, struct mg_rpc_request *req)
{
   
   if (*gDEBUG) {
      printf("RPC::GetValve called\n");
   }
   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,", atoi(req->id->ptr));
   sprintf(buf+strlen(buf), "\"valve\": %1.2lf", fHistory->GetLastEntry()->fValve);
   sprintf(buf+strlen(buf), "}\n");
   return strlen(buf);
}

int RPC::GetHistory(char *buf, int len, struct mg_rpc_request *req)
{
   if (*gDEBUG) {
      printf("RPC::GetHistory called\n");
   }
   //history selection:
   int nEntries = atoi(req->params[1].ptr);
   
   if (nEntries > 1000 || nEntries == 0) nEntries = 1000;
   
   
   int time1 = atoi(req->params[2].ptr);
   int time2 = atoi(req->params[3].ptr);
   
   
   std::vector<historyEntry> hist;
   if (time2 > time1) {
      hist = fHistory->GetHistory(nEntries, time1, time2);
   } else {
      hist = fHistory->GetHistory(nEntries, time1);
   }
   double maxT = 0, minT = 50;
   for (unsigned int i = 0; i < hist.size(); ++i) {
      if (hist[i].fAirTemp > maxT) maxT = hist[i].fAirTemp;
      if (hist[i].fHutTemp > maxT) maxT = hist[i].fHutTemp;
      if (hist[i].fLM35Temp > maxT) maxT = hist[i].fLM35Temp;
      if (hist[i].fAirReqTemp > maxT) maxT = hist[i].fAirReqTemp;
      if (hist[i].fHutReqTemp > maxT) maxT = hist[i].fHutReqTemp;
      if (hist[i].fAirTemp < minT) minT = hist[i].fAirTemp;
      if (hist[i].fHutTemp < minT) minT = hist[i].fHutTemp;
      if (hist[i].fLM35Temp < minT) minT = hist[i].fLM35Temp;
      if (hist[i].fAirReqTemp < minT) minT = hist[i].fAirReqTemp;
      if (hist[i].fHutReqTemp < minT) minT = hist[i].fHutReqTemp;
   }
   if (maxT < minT) {
      double t = maxT;
      maxT = minT;
      minT = t;
   }
   if (maxT - minT < 1) {
      double t = 0.5 * (maxT + minT);
      maxT = t + 0.5;
      minT = t - 0.5;
   }
   maxT += 0.05 * (maxT - minT);
   minT -= 0.05 * (maxT - minT);
   
   int t0 = 0;
   int t1 = 1;
   if (hist.size()) {
      t1 = hist[0].fTime;
      t0 = hist[hist.size()-1].fTime;
      if (t1 - t0 < 1) {
         t1 = t0 + 1;
      }
   }
   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,", atoi(req->id->ptr));
   sprintf(buf+strlen(buf), " \"minTemp\": %f, \"maxTemp\": %f, \"nEntries\": %d, ", minT, maxT, (int)hist.size());
   sprintf(buf+strlen(buf), " \"minTime\": %d, \"maxTime\": %d, ", t0, t1);
   
   if (*gDEBUG) {
      printf("minTime: %d, maxTime: %d\n", t0, t1);
      printf("minTemp: %f, maxTemp: %f\nnEntries: %d\n", minT, maxT, (int)hist.size());
   }
   
   sprintf(buf+strlen(buf), "\"history\": [");
   double vTime = 0;
   double vValve = 0;
   double vAirT = 0;
   double vLM35 = 0;
   double vAirReqT = 0;
   double vHutT = 0;
   double vHutReqT = 0;
   if (hist.size()) {
      unsigned int iEntry = 0;
      do {
         historyEntry* anEntry = &hist[iEntry];
         
         vTime = (anEntry->fTime - t0) * 1.0 / (t1 - t0);
         vValve = anEntry->fValve / 100.;
         vAirT = (anEntry->fAirTemp - minT) / (maxT - minT);
         vLM35 = (anEntry->fLM35Temp - minT) / (maxT - minT);
         vAirReqT = (anEntry->fAirReqTemp - minT) / (maxT - minT);
         vHutT = (anEntry->fHutTemp - minT) / (maxT - minT);
         vHutReqT = (anEntry->fHutReqTemp - minT) / (maxT - minT);
         
         
         sprintf(buf + strlen(buf), "[%0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f]",
                 vTime, vValve, vAirT, vAirReqT, vHutT, vHutReqT, vLM35);
         if (*gDEBUG > 1) {
            printf("[%0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f, %0.4f]\n",
                   vTime, vValve, vAirT, vAirReqT, vHutT, vHutReqT, vLM35);
         }
         ++iEntry;
      } while (iEntry < hist.size() && sprintf(buf + strlen(buf), ","));
   }
   sprintf(buf + strlen(buf), "]}\n");
   //   printf("%s", buf);
   //   printf(" .... done \n");
   return strlen(buf);
}

int RPC::SetActive(char *buf, int len, struct mg_rpc_request *req)
{
   
   int controlID = atoi(req->params[1].ptr);
   int active    = atoi(req->params[2].ptr);
   
   if (*gDEBUG) {
      printf("PID Values Changed: control %i active %i\n", controlID, active);
   }
   
   switch (controlID) {
      case 0:
      // Air control
      fPID0->isActive = active;
      break;
      case 1:
      // Hut control
      fPID1->isActive = active;
   }
   
   //Write History
   fShared->SetCommand(1);
   
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::SetAllActive(char *buf, int len, struct mg_rpc_request *req)
{
   int active = atoi(req->params[1].ptr);
   
   if (*gDEBUG) {
      printf("Set All Active: %d\n", active);
   }
   
   fPID0->isActive = active;
   fPID1->isActive = active;
   
   if (!active) {
      //Close Valve and write status
      fShared->SetCommand(2, 0);
   }
      
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::SetTemp(char *buf, int len, struct mg_rpc_request *req)
{
   int controlID = atoi(req->params[1].ptr);
   double temp   = atof(req->params[2].ptr);
   
   if (*gDEBUG) {
      printf("PID Values Changed: control %i temp %1.2lf\n", controlID, temp);
   }
   switch (controlID) {
      case 0:
      // Water control
      fPID0->reqTemp = temp;
      break;
      case 1:
      // Hut control
         fPID1->reqTemp = temp;
   }
   
   //Write Status
   fShared->SetCommand(1);
   
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}


// For Experts

int RPC::SetPID(char *buf, int len, struct mg_rpc_request *req)
{
   if (*gDEBUG) {
      printf("old values: \n");
      printf("Air (p, i, d, t): %f %f %f\n", fPID0->P, fPID0->I, fPID0->D);
      printf("Hut (p, i, d, t): %f %f %f\n", fPID1->P, fPID1->I, fPID1->D);
   }
   int mode = atoi (req->params[1].ptr);
   if (mode == 0) {
      fPID0->P = atof(req->params[2].ptr);
      double newI = atof(req->params[3].ptr);
      if (newI != 0 && fPID0->I != 0) {
         fPID0->intDiff *= fPID0->I / newI;
      }
      fPID0->I = newI;
      fPID0->D = atof(req->params[4].ptr);
      fPID1->P = atof(req->params[5].ptr);
      newI = atof(req->params[6].ptr);
      if (newI != 0 && fPID1->I != 0) {
         fPID1->intDiff *= fPID1->I / newI;
      }
      fPID1->I = newI;
      fPID1->D = atof(req->params[7].ptr);
   } else if (mode == 1) {
      fPID0->P = atof(req->params[2].ptr);
   } else if (mode == 2) {
      fPID0->I = atof(req->params[2].ptr);
   } else if (mode == 3) {
      fPID0->D = atof(req->params[2].ptr);
   } else if (mode == 4) {
      fPID1->P = atof(req->params[2].ptr);
   } else if (mode == 5) {
      fPID1->I = atof(req->params[2].ptr);
   } else if (mode == 6) {
      fPID1->D = atof(req->params[2].ptr);
   }
   
   //Write Status
   fShared->SetCommand(1);
   
   if (*gDEBUG) {
      printf("new values: \n");
      printf("Air (p, i, d, t): %f %f %f\n", fPID0->P, fPID0->I, fPID0->D);
      printf("Hut (p, i, d, t): %f %f %f\n", fPID1->P, fPID1->I, fPID1->D);
   }
   
   
   
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}


int RPC::SetValve(char *buf, int len, struct mg_rpc_request *req)
{
   float v;
   
   v = atoi(req->params[1].ptr);
   
   fShared->SetCommand(2, v);
   
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}


int RPC::DumpHistory(char *buf, int len, struct mg_rpc_request *req) {
   
   int nEntries = atoi(req->params[1].ptr);
   int time = atoi(req->params[2].ptr);
   int toFile = atoi(req->params[3].ptr);
   
   fHistory->Dump(nEntries, time, toFile);
   
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::SetDebug(char* buf, int len, struct mg_rpc_request *req) {
   int debug = atoi(req->params[1].ptr);
   printf("Debugging Level %d\n", debug);
   *gDEBUG = atoi(req->params[1].ptr);
   fShared->SetDebug(*gDEBUG);
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::GetDebug(char* buf, int len, struct mg_rpc_request *req) {
   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,", atoi(req->id->ptr));
   sprintf(buf + strlen(buf), " \"debug\" : %d", fShared->GetDebug());
   sprintf(buf + strlen(buf), "}\n");
   return strlen(buf);
}

int RPC::SetStatusLow(char* buf, int len, struct mg_rpc_request *req) {
   printf("Stat Low set to %s %f\n", req->params[1].ptr, atof(req->params[1].ptr));
   fShared->GetHistoryStatusParameters()->low = atof(req->params[1].ptr);
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::SetStatusTime(char* buf, int len, struct mg_rpc_request *req) {
   int indx = atoi(req->params[1].ptr);
   if (indx >= 0 && indx < NUM_HIST_STAT) {
      fShared->GetHistoryStatusParameters()->timespan[indx] = atof(req->params[2].ptr);
   }
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::SetStatusUp(char* buf, int len, struct mg_rpc_request *req) {
   int indx = atoi(req->params[1].ptr);
   if (indx >= 0 && indx < NUM_HIST_STAT) {
      fShared->GetHistoryStatusParameters()->upLim[indx] = atof(req->params[2].ptr);
   }
   return mg_rpc_create_reply(buf, len, req, "i", 1);
}

int RPC::GetStatusParameters(char* buf, int len, struct mg_rpc_request *req) {

   historyStatusParameters* pars = fShared->GetHistoryStatusParameters();

   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,", atoi(req->id->ptr));
   sprintf(buf + strlen(buf), " \"low\" : %f,", pars->low);
   sprintf(buf + strlen(buf), " \"span\" : [");
   int i = 0;
   do {
      sprintf(buf + strlen(buf), "%f", pars->timespan[i]);
   } while (++i < NUM_HIST_STAT && sprintf(buf + strlen(buf), ", "));
   sprintf(buf + strlen(buf), "], \"up\" : [");
   i = 0;
   do {
      sprintf(buf + strlen(buf), "%f", pars->upLim[i]);
   } while (++i < NUM_HIST_STAT && sprintf(buf + strlen(buf), ", "));
   sprintf(buf + strlen(buf), "]}\n");
   return strlen(buf);
}

int RPC::GetStatus(char* buf, int len, struct mg_rpc_request *req) {
   int index = 0;
   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,", atoi(req->id->ptr));
   sprintf(buf + strlen(buf), " \"status\" : %f,", fHistory->GetOffsetStatus(&index));
   sprintf(buf + strlen(buf), " \"index\" : %d", index);
   sprintf(buf + strlen(buf), "}\n");
   return strlen(buf);
}

void RPC::curlHandler(struct mg_connection* nc, int ev, void *ev_data)
{
   char buf[512];
   
   struct http_message *hm = (struct http_message*) ev_data;
   if (*gDEBUG) {
      printf("%.*s\n",(int)hm->message.len, hm->message.p);
      printf("-- Body -- \n%.*s\n",(int)hm->body.len, hm->body.p);
   }
   historyEntry* anEntry = fHistory->GetLastEntry();
   
   sprintf(buf, "HTTP/1.0 200 OK\r\n\r\n");
   if (mg_vcmp(&hm->uri, "/curl/time") == 0) {
      sprintf(buf + strlen(buf), "%ld\n", anEntry->fTime);
   } else if (mg_vcmp(&hm->uri, "/curl/valve") == 0) {
      sprintf(buf + strlen(buf), "%1.1f\n", anEntry->fValve);
      if (mg_vcmp(&hm->method, "POST") == 0) {
         double val = atof(hm->body.p);
         fShared->SetCommand(2, val);
         if (*gDEBUG) {
            printf("valve set to %f\n", val);
         }
      }
   } else if (mg_vcmp(&hm->uri, "/curl/air") == 0) {
      sprintf(buf + strlen(buf), "%1.1f\n", anEntry->fAirTemp);
   } else if (mg_vcmp(&hm->uri, "/curl/airReq") == 0) {
      sprintf(buf + strlen(buf), "%1.1f\n", anEntry->fAirReqTemp);
      if (mg_vcmp(&hm->method, "POST") == 0) {
         double val = atof(hm->body.p);
         fPID0->reqTemp = val;
         if (*gDEBUG) {
            printf("Air requested temperature set to %f\n", val);
         }
      }
   } else if (mg_vcmp(&hm->uri, "/curl/hut") == 0) {
      sprintf(buf + strlen(buf), "%1.1f\n", anEntry->fHutTemp);
   } else if (mg_vcmp(&hm->uri, "/curl/hutReq") == 0) {
      sprintf(buf + strlen(buf), "%1.1f\n", anEntry->fHutReqTemp);
      if (mg_vcmp(&hm->method, "POST") == 0) {
         double val = atof(hm->body.p);
         fPID1->reqTemp = val;
         if (*gDEBUG) {
            printf("Hut requested temperature set to %f\n", val);
         }
      }
   } else if (mg_vcmp(&hm->uri, "/curl/LM35") == 0) {
      sprintf(buf + strlen(buf), "%1.1f\n", anEntry->fLM35Temp);
   } else if (mg_vcmp(&hm->uri, "/curl") == 0) {
      sprintf(buf + strlen(buf), "time: %ld\n", anEntry->fTime);
      sprintf(buf + strlen(buf), "valve: %1.1f\n", anEntry->fValve);
      sprintf(buf + strlen(buf), "air: %1.1f\n", anEntry->fAirTemp);
      sprintf(buf + strlen(buf), "airReq: %1.1f\n", anEntry->fAirReqTemp);
      sprintf(buf + strlen(buf), "hut: %1.1f\n", anEntry->fHutTemp);
      sprintf(buf + strlen(buf), "hutReq: %1.1f\n", anEntry->fHutReqTemp);
      sprintf(buf + strlen(buf), "LM35: %1.1f\n", anEntry->fLM35Temp);
   } else {
      sprintf(buf + strlen(buf), "-1\nUnkown Option: %.*s\n", (int)hm->uri.len, hm->uri.p);
      sprintf(buf + strlen(buf), "Available Options: /time, /valve, /air, /airReq, /hut, /hutReq, /LM35\n");
   }
   mg_printf(nc, "%s", buf);
   nc->flags |= MG_F_SEND_AND_CLOSE;
}
