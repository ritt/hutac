//
//  history.c
//  
//
//  Created by Patrick Schwendimann on 12.12.18.
//

#include "historyReader.h"
#include "historyEntry.h"
#include "server.h"
#include "shared.h"
#include <math.h>
#include <sys/time.h>

HistoryReader::HistoryReader(sharedMemory* shared)
{
   fHistory = shared->GetHistory();
   fHistorySize = shared->GetHistorySize();
   fCurrentIndex = shared->GetCurrentIndex();
   fCycleCompleted = shared->GetCycleCompleted();
   fStatPar = shared->GetHistoryStatusParameters();
}

HistoryReader::~HistoryReader()
{
   
}

std::vector<historyEntry> HistoryReader::GetHistory(long nEntries, long tStart, long tEnd)
{
   if (*gDEBUG) {
      printf("History::GetHistory(%ld, %ld, %ld)\n", nEntries, tStart, tEnd);
   }
   std::vector<historyEntry> selection;
   long firstIndex = (*fCycleCompleted) ? *fCurrentIndex : 0;
   long lastIndex = *fCurrentIndex - 1;
   if (lastIndex == -1 && *fCycleCompleted) lastIndex = *fHistorySize - 1;
   if (*gDEBUG) {
      printf("first %ld, last %ld, current %ld, size %ld\n", firstIndex, lastIndex, *fCurrentIndex, *fHistorySize);
   }
   if (tStart < fHistory[firstIndex].fTime) {
      tStart = fHistory[firstIndex].fTime;
   }
   
   long time = tEnd - tStart;
   
   if (*gDEBUG) {
      printf("tStart %ld, tEnd %ld, time %ld\n", tStart, tEnd, time);
   }
   if (*gDEBUG > 1) {
      Dump(0,0,0);
   }
   
   
   double tNext = 0;
   double dt = 0;
   if (time) {
      dt = nEntries ? time * 1. / nEntries : 0;
      tNext = dt ? ceil(tEnd / dt) * dt : tEnd;
   } else if (lastIndex != -1){
      tStart = fHistory[firstIndex].fTime;
      tEnd = fHistory[lastIndex].fTime;
      dt = nEntries ? ceil((tEnd - tStart) * 1. / nEntries) : 0;
      tNext = dt ? ceil(tEnd / dt) * dt : tEnd;
   }
   
   //if nEntries == 0, get all Entries
   if (!nEntries) nEntries = (*fCycleCompleted) ? *fHistorySize : lastIndex - firstIndex;
   
   long index = lastIndex;
   if (*gDEBUG) {
      printf("GetHistory - mainloop\n");
   }
   
   //locate the first entry to use
   while (fHistory[index].fTime > tNext && index != firstIndex) {
      --index;
      if (index < 0) index += *fHistorySize;
   }
   
   //assert to be in the correct bin
   if (dt) {
      while (fHistory[index].fTime < tNext) tNext -= dt;
   }
   
   historyEntry avrgEntry = {0, 0, 0, 0, 0, 0};
   int nAve = 0;
   //mainloop
   while (fHistory[index].fTime > tStart && index != firstIndex) {
      avrgEntry.fTime = 0;
      avrgEntry.fValve = 0;
      avrgEntry.fAirTemp = 0;
      avrgEntry.fAirReqTemp = 0;
      avrgEntry.fHutTemp = 0;
      avrgEntry.fHutReqTemp = 0;
      avrgEntry.fLM35Temp = 0;
      nAve = 0;
      while (fHistory[index].fTime > tNext && index != firstIndex) {
         avrgEntry *= nAve / (nAve + 1.);
         ++nAve;
         avrgEntry += fHistory[index] / nAve;
         --index;
         if (index < 0) {
            if (*gDEBUG) {
               printf ("index < 0\n");
            }
            index += *fHistorySize;
         }
         if (*gDEBUG) {
            printf("begin index %ld\n", index);
         }
      }
      if (nAve) {
         if (*gDEBUG) {
            printf("nAve %d, avrgEntry (%ld, %f, %f, %f, %f, %f, %f)\n",
                   nAve,
                   avrgEntry.fTime,
                   avrgEntry.fValve,
                   avrgEntry.fAirTemp,
                   avrgEntry.fAirReqTemp,
                   avrgEntry.fHutTemp,
                   avrgEntry.fHutReqTemp,
                   avrgEntry.fLM35Temp);
         }
         selection.push_back(avrgEntry);
      } else {
         selection.push_back(fHistory[index]);
      }
      
      if (index != firstIndex) {
         if (dt) {
            while (fHistory[index].fTime <= tNext) {
               tNext -= dt;
            }
         } else {
            --index;
            if (index < 0) {
               index += *fHistorySize;
            }
            if (*gDEBUG) {
               printf("end index %ld\n", index);
            }
         }
      }
   }
   if (*gDEBUG) {
      printf("End of History::GetHistory\n");
   }
   return selection;
}

std::vector<historyEntry> HistoryReader::GetHistory(long nEntries, long time) {
   struct timeval now;
   gettimeofday(&now, 0);
   return GetHistory(nEntries, now.tv_sec - time, now.tv_sec);
}

void HistoryReader::Dump(long nEntries, long time, bool toFile = false)
{
   struct timeval now;
   gettimeofday(&now, 0);
   double tNext;
   double dt;
   FILE* dumpFile = 0;
   
   long firstIndex = (*fCycleCompleted) ? *fCurrentIndex : 0;
   long lastIndex = *fCurrentIndex - 1;
   if (lastIndex == -1 && *fCycleCompleted) lastIndex = *fHistorySize - 1;
   
   if (time) {
      tNext = now.tv_sec - time;
      dt = nEntries ? time * 1. / nEntries : 0;
   } else {
      tNext = (lastIndex != -1) ? fHistory[firstIndex].fTime : 0;
      dt = nEntries ? (now.tv_sec - tNext) * 1. / nEntries : 0;
   }
   
   //if nEntries == 0, get all Entries
   if (!nEntries) nEntries = (*fCycleCompleted) ? *fHistorySize : lastIndex - firstIndex;
   
   if (toFile) {
      dumpFile = fopen("HistoryDump.txt", "w");
   }
   
   struct tm *aTime = 0;
   char timeBuf[10];
   char dateBuf[16];
   if (toFile) {
      fprintf(dumpFile, "Index\tDate\t\tTime\t\tValve\t\tT air\t\tT air req\t T hut\t\t T hut req\t\tT LM35\n");
   } else {
      printf("Index\tDate\t\tTime\t\tValve\t\tT air\t\tT air req\t T hut\t\t T hut req\t\tT LM35\n");
   }
   
   long i = firstIndex;
   for (long iEntries = 0; (iEntries < nEntries) && (i != firstIndex || iEntries == 0); ++iEntries) {
      while (i != lastIndex && fHistory[i].fTime < tNext) {
         ++i;
         if (i >= *fHistorySize) {
            i -= *fHistorySize;
         }
      }
      aTime = localtime(&fHistory[i].fTime);
      
      strftime(timeBuf, sizeof timeBuf, "%H:%M:%S", aTime);
      strftime(dateBuf, sizeof dateBuf, "%Y-%m-%d", aTime);
      if (toFile) {
         fprintf(dumpFile, "%ld \t %s \t %s \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f\n",
                 i,
                 dateBuf,
                 timeBuf,
                 fHistory[i].fValve,
                 fHistory[i].fAirTemp,
                 fHistory[i].fAirReqTemp,
                 fHistory[i].fHutTemp,
                 fHistory[i].fHutReqTemp,
                 fHistory[i].fLM35Temp);
      } else {
         printf("%ld \t %s \t %s \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f\n",
                i,
                dateBuf,
                timeBuf,
                fHistory[i].fValve,
                fHistory[i].fAirTemp,
                fHistory[i].fAirReqTemp,
                fHistory[i].fHutTemp,
                fHistory[i].fHutReqTemp,
                fHistory[i].fLM35Temp);
      }
      if (dt) {
         while (fHistory[i].fTime >= tNext) tNext += dt;
      } else {
         ++i;
         if (i >= *fHistorySize) {
            i -= *fHistorySize;
         }
      }
      
   }
   if (toFile) {
      fclose(dumpFile);
   } else {
      printf("----\n");
   }
}

historyEntry *HistoryReader::GetLastEntry()
{
   historyEntry *retVal = NULL;
   if (*fCurrentIndex != 0 || *fCycleCompleted) {
      long indx = *fCurrentIndex ? *fCurrentIndex - 1 : *fHistorySize -1;
      retVal = fHistory + indx;
   }
   return retVal;
}

float HistoryReader::GetOffsetStatus(int *id)
{
   long firstIndex = (*fCycleCompleted) ? *fCurrentIndex : 0;
   long lastIndex = *fCurrentIndex - 1;
   if (lastIndex == -1 && *fCycleCompleted) lastIndex = *fHistorySize - 1;
   if (lastIndex == -1) {
      // there is no history recorded, return 0
      return 0;
   }

   long index = lastIndex;
   
   int nEntries[NUM_HIST_STAT];
   float sum[NUM_HIST_STAT];
   for (int i = 0; i < NUM_HIST_STAT; ++i) {
      nEntries[i] = 0;
      sum[i] = 0;
   }
   long lastTime = fHistory[lastIndex].fTime;

   float maxTimespan = 0;
   float timespan = 0;
   float diff = 0;
   float retVal = 0;
   float aVal = 0;

   for (int i = 0; i < NUM_HIST_STAT; ++i) {
      if (maxTimespan < fStatPar->timespan[i]) maxTimespan = fStatPar->timespan[i];
   }
   if (*gDEBUG) {
      printf("maxTimespan: %f\n", maxTimespan);
   }
   // mainloop
   timespan = lastTime - fHistory[index].fTime;
   while (timespan < maxTimespan && index != firstIndex) {
      diff = fabs(fHistory[index].fHutTemp - fHistory[index].fHutReqTemp);
      if (diff < fStatPar->low) {
         // ignore deviations below a certain threshold
         diff = 0;
      }
      if (*gDEBUG) {
         printf("timespan: %f, index: %ld, diff: %f\n", timespan, index, diff);
      }
      for (int i = 0; i < NUM_HIST_STAT; ++i) {
         if (timespan < fStatPar->timespan[i]) {
            if (diff < 3 * fStatPar->upLim[i]) {
               sum[i] += diff;
            } else {
               sum[i] += 3 * fStatPar->upLim[i];
            }
            ++nEntries[i];
         }
         if (*gDEBUG) {
            printf("i: %d, sum: %f, nEntries: %d\n", i, sum[i], nEntries[i]);
         }
      }
      --index;
      if (index < 0) index += *fHistorySize;
      timespan = lastTime - fHistory[index].fTime;
   }
   for (int i = 0; i < NUM_HIST_STAT; ++i) {
      if (*gDEBUG) {
         printf("i: %d, nEntries: %d, sum: %f\n", i, nEntries[i], sum[i]);
      }
      if (nEntries[i] > 0 && fStatPar->upLim[i] > 0) {
         aVal = sum[i] /(nEntries[i] * fStatPar->upLim[i]);
         if (aVal > retVal) {
            if (id) {
               *id = i;
            }
            retVal = aVal;
         }
      }
   }

   return retVal;
}
