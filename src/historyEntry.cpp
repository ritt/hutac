//
//  historyEntry.cpp
//  
//
//  Created by Patrick Schwendimann on 12.12.18.
//

#include "historyEntry.h"

historyEntry operator+= (historyEntry &lhs, historyEntry rhs) {
   lhs.fTime += rhs.fTime;
   lhs.fValve += rhs.fValve;
   lhs.fAirTemp += rhs.fAirTemp;
   lhs.fAirReqTemp += rhs.fAirReqTemp;
   lhs.fHutTemp += rhs.fHutTemp;
   lhs.fHutReqTemp += rhs.fHutReqTemp;
   lhs.fLM35Temp += rhs.fLM35Temp;
   return lhs;
}

historyEntry operator/ (historyEntry lhs, int n) {
   lhs.fTime /= n;
   lhs.fValve /= n;
   lhs.fAirTemp /= n;
   lhs.fAirReqTemp /= n;
   lhs.fHutTemp /= n;
   lhs.fHutReqTemp /= n;
   lhs.fLM35Temp /= n;
   return lhs;
}

historyEntry operator*= (historyEntry &lhs, double d) {
   lhs.fTime *= d;
   lhs.fValve *= d;
   lhs.fAirTemp *= d;
   lhs.fAirReqTemp *= d;
   lhs.fHutTemp *= d;
   lhs.fHutReqTemp *= d;
   lhs.fLM35Temp *= d;
   return lhs;
}
