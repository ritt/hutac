//
// adcdac.c - command line interface for PSI RPi_DAC_ADC_IO board
//

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#ifdef __ARMEL__

// This code is a simple example to test the top board of the
// Raspberry Pi. It makes no sense to use it on any other device.

#include <wiringPi.h>
#include <wiringPiSPI.h>


int spi_fd0, spi_fd1;

void setdac(int channel, double v)
{
   unsigned int d;
   unsigned char spi_buf[3];

   if (v > 10)
      v = 10;
   if (v < 0)
      v = 0;
   d = (unsigned int) (v / 10 * 65535);

   // program DAC ranges
   spi_buf[0] = 0x0C; // Output range select register, both DACs
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x01; // Range = +10V
   wiringPiSPIDataRW(spi_fd0, spi_buf, 3);

   // program DAC power register
   spi_buf[0] = 0x10; // Power control register
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x05; // PUA = PUB = 1
   wiringPiSPIDataRW(spi_fd0, spi_buf, 3);

   if (channel == -1 || channel == 0) {
      // set DAC0 output
      spi_buf[0] = 0x00;
      spi_buf[1] = d >> 8;   // MSB
      spi_buf[2] = d & 0xFF; // LSB
      wiringPiSPIDataRW(spi_fd0, spi_buf, 3);
   }   

   if (channel == -1 || channel == 1) {
      // set DAC1 output
      spi_buf[0] = 0x02;
      spi_buf[1] = d >> 8;   // MSB
      spi_buf[2] = d & 0xFF; // LSB
      wiringPiSPIDataRW(spi_fd0, spi_buf, 3);
   }

   if (channel == -1)
      printf("Set both DACs to %lgV\n", v);
   else
      printf("Set DAC%d to %lgV\n", channel, v);
}

void readadc(int channel)
{
   int ch;
   double v[4];
   unsigned char spi_buf[4];

   spi_buf[0] = 0xC0;
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x00;
   spi_buf[3] = 0x00; // Manual Ch 0 Conversion
   wiringPiSPIDataRW(spi_fd1, spi_buf, 4);

   for (ch = 0 ; ch<4 ; ch++) {
     spi_buf[0] = 0xC0 + (ch+1)*0x04;
      spi_buf[1] = 0x00;
      spi_buf[2] = 0x00;
      spi_buf[3] = 0x00; // Manual Ch 'c' Conversion

      wiringPiSPIDataRW(spi_fd1, spi_buf, 4);

      // convert to Volts
      v[ch] = ((spi_buf[2] << 8) | spi_buf[3])/65535.0 * 20.48 - 10.24;

      // round to three digit
      v[ch] = (int)(v[ch]*1000+0.5)/1000.0;

      if (channel == -1 || channel == ch)
         printf("%1.3lf ", v[ch]);
   }

   printf("\n");
}

#endif
int main(int argc, char *argv[])
{
#ifdef __ARMEL__
   int i;
   unsigned char spi_buf[3];
   int ch = -1;
   double v = -1; 

   wiringPiSetup();
   spi_fd0 = wiringPiSPISetup(0, 10000000); // DAC 10 MHz
   spi_fd1 = wiringPiSPISetup(1, 10000000); // ADC 10 MHz
   if (spi_fd0 < 0 || spi_fd1 < 0) {
      perror("Error on wiringPiSPISetup");
      return 1;
   }

   for (i=1 ; i<argc ; i++) {
      if (i < argc-1 && argv[i][0] == '-' && argv[i][1] == 'c')
	 ch = atoi(argv[++i]);
      else if (isdigit(argv[i][0]))
	 v = atof(argv[i]);
      else if (argc == 1)
	ch = -1;
      else {
         printf("usage: adcdac [-c x] [voltage]\n\n");
         printf("       -c x      Channel 0/1 for DAC, 0/1/2/3 for ADC.\n");
         printf("       voltage   If given, set DAC value, otherwise read back ADC voltages.\n");
         return 1;
      }
   }
   
   if (v >= 0) {
      if (ch < -1 || ch > 1) {
	 printf("Channel must be 0 or 1\n");
	 return 1;
      }
      setdac(ch, v);
   } else {
      if (ch < -1 || ch > 3) {
	 printf("Channel must be between 0 and 3\n");
	 return 1;
      }
      readadc(ch);
   } 
#else
   printf("Not compiled on a Raspberry Pi\n");
   printf("Terminating immediately\n");
#endif
   
   return 0;
}
