// server-HV.c - Simple JSON-RPC based HV server

#include <stdio.h>
#ifdef __ARMEL__

// libs for Raspberry Pi here

#include <wiringPi.h>
#include <wiringPiSPI.h>

#endif
#include "mongoose.h"

static struct mg_serve_http_opts s_http_server_opts;
const char *s_http_port = "8080";

int spi_fd0, spi_fd1;

static int rpc_setvalve(char *buf, int len, struct mg_rpc_request *req)
{
#ifdef __ARMEL__
   double v;
   unsigned int d;
   unsigned char spi_buf[3];

   v = atoi(req->params[1].ptr) / 10; // convert percent to volts
   if (v > 10)
      v = 10;
   if (v < 0)
      v = 0;
   d = (unsigned int) (v / 10 * 65535);

   // program DAC ranges
   spi_buf[0] = 0x0C; // Output range select register, both DACs
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x01; // Range = +10V
   wiringPiSPIDataRW(spi_fd0, spi_buf, 3);

   // program DAC power register
   spi_buf[0] = 0x10; // Power control register
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x05; // PUA = PUB = 1
   wiringPiSPIDataRW(spi_fd0, spi_buf, 3);

   // set DAC0 output
   spi_buf[0] = 0x00;
   spi_buf[1] = d >> 8;   // MSB
   spi_buf[2] = d & 0xFF; // LSB
   wiringPiSPIDataRW(spi_fd0, spi_buf, 3);
#endif
   // printf("Set DAC0 to %lgV\n", v);
   return mg_rpc_create_reply(buf, len, req, "i", 1);;
}

static int rpc_readtemp(char *buf, int len, struct mg_rpc_request *req)
{
   int ch;
   double v[4];
#ifdef __ARMEL__
   unsigned char spi_buf[4];

   spi_buf[0] = 0xC0;
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x00;
   spi_buf[3] = 0x00; // Manual Ch 0 Conversion
   wiringPiSPIDataRW(spi_fd1, spi_buf, 4);

   for (ch = 0 ; ch<4 ; ch++) {
      spi_buf[0] = 0xC0 + (ch+1)*0x04;
      spi_buf[1] = 0x00;
      spi_buf[2] = 0x00;
      spi_buf[3] = 0x00; // Manual Ch 'c' Conversion

      wiringPiSPIDataRW(spi_fd1, spi_buf, 4);

      // convert to Volts
      v[ch] = ((spi_buf[2] << 8) | spi_buf[3])/65535.0 * 20.48 - 10.24;

      // round to three digit
      v[ch] = (int)(v[ch]*1000+0.5)/1000.0;
   }
#else
   for (ch = 0 ; ch<4 ; ch++) {
      v[ch] = ch;
   }
#endif

   sprintf(buf, "{\"jsonrpc\":\"2.0\",\"id\":%d,\"result\":[", atoi(req->id->ptr));
   sprintf(buf+strlen(buf), "%1.2lf, %1.2lf", v[0]*10, v[1]*10);
   sprintf(buf+strlen(buf), "]}\n");
   return strlen(buf);
}


static void ev_handler(struct mg_connection *nc, int ev, void *ev_data)
{
   struct http_message *hm = (struct http_message *) ev_data;
   static const char *methods[] = { "setvalve", "readtemp", NULL };
   static mg_rpc_handler_t handlers[] = { rpc_setvalve, rpc_readtemp, NULL };
   char buf[1000];

   if (ev == MG_EV_HTTP_REQUEST) {
      if (mg_vcmp(&hm->uri, "/json-rpc") == 0) {
         mg_rpc_dispatch(hm->body.p, hm->body.len, buf, sizeof(buf),
                         methods, handlers);
         mg_printf(nc, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\n"
                   "Content-Type: application/json\r\n\r\n%s",
                   (int) strlen(buf), buf);
         nc->flags |= MG_F_SEND_AND_CLOSE;
      } else {
         // serve static content
         mg_serve_http(nc, hm, s_http_server_opts);
      }
   }
}

int main(int argc, char *argv[])
{
   struct mg_mgr mgr;
   struct mg_connection *nc;

#ifdef __ARMEL__
   unsigned char spi_buf[3];

   wiringPiSetup();
   spi_fd0 = wiringPiSPISetup(0, 10000000); // DAC 10 MHz
   spi_fd1 = wiringPiSPISetup(1, 10000000); // ADC 10 MHz
   if (spi_fd0 < 0 || spi_fd1 < 0) {
      perror("Error on wiringPiSPISetup");
      exit(1);
   }

   // program DAC ranges
   spi_buf[0] = 0x0C; // Output range select register, both DACs
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x01; // Range = +10V
   wiringPiSPIDataRW(spi_fd0, spi_buf, 3);

   // program DAC power register
   spi_buf[0] = 0x10; // Power control register
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x05; // PUA = PUB = 1
   wiringPiSPIDataRW(spi_fd0, spi_buf, 3);
#endif

   mg_mgr_init(&mgr, NULL);
   nc = mg_bind(&mgr, s_http_port, ev_handler);
   if (nc == NULL) {
      fprintf(stderr, "Error starting server on port %s\n", s_http_port);
      exit(1);
   }

   mg_set_protocol_http_websocket(nc);
   s_http_server_opts.document_root = ".";
   s_http_server_opts.enable_directory_listing = "yes";

   printf("Starting server on port %s\n", s_http_port);

   while(1) {
      mg_mgr_poll(&mgr, 1000);
   }

   return 0;
}
