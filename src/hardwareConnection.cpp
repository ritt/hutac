#include <stdio.h>
#include <stdlib.h>
#include "history.h"
#include "main.h"
#include "hardwareConnection.h"

#ifdef __ARMEL__

// libs for Raspberry Pi here

#include <wiringPi.h>
#include <wiringPiSPI.h>

#endif

HWConnection::HWConnection()
{
   fArraySize = 1000;
   fCycles = 20;
   fIntegrationWindow = 1;
   fIndex = 0;
   for (unsigned int i = 0; i < 4; ++i) {
      fTemperatureArray[i] = new double[fArraySize];
   }
   fValve = 0;
   fTemperature[0] = 26.;
   fTemperature[1] = 25.;
   fTemperature[2] = 24.;
   fTemperature[3] = 23.;
   
   fConversionFactor[0] = 10.;
   fConversionFactor[1] = 10.;
   fConversionFactor[2] = 100.;
   fConversionFactor[3] = 100.;
   
   printf("Hardware Connection Initialisation\n");
#ifdef __ARMEL__
   unsigned char spi_buf[3];
   
   wiringPiSetup();
   fspi_fd0 = wiringPiSPISetup(0, 10000000); // DAC 10 MHz
   fspi_fd1 = wiringPiSPISetup(1, 10000000); // ADC 10 MHz
   if (fspi_fd0 < 0 || fspi_fd1 < 0) {
      perror("Error on wiringPiSPISetup");
      exit(1);
   }
   
   // program DAC ranges
   spi_buf[0] = 0x0C; // Output range select register, both DACs
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x01; // Range = +10V
   wiringPiSPIDataRW(fspi_fd0, spi_buf, 3);
   
   // program DAC power register
   spi_buf[0] = 0x10; // Power control register
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x05; // PUA = PUB = 1
   wiringPiSPIDataRW(fspi_fd0, spi_buf, 3);
   
   //read the temperature and fill the entire array
   ReadTemperatures(fArraySize);
#endif
}

HWConnection::~HWConnection()
{
   for (int ch = 0; ch < 4; ++ch) {
      delete[] fTemperatureArray[ch];
   }
   
}

void HWConnection::SetValve(double value)
{
   
   if (value > 100) value = 100;
   if (value < 0) value = 0;
   fValve = value;
   
#ifdef __ARMEL__
   double v;
   unsigned int d;
   unsigned char spi_buf[3];
   
   v = value / 10; // convert percent to volts
   if (v > 10)
      v = 10;
   if (v < 0)
      v = 0;
   d = (unsigned int) (v / 10 * 65535);
   
   // program DAC ranges
   spi_buf[0] = 0x0C; // Output range select register, both DACs
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x01; // Range = +10V
   wiringPiSPIDataRW(fspi_fd0, spi_buf, 3);
   
   // program DAC power register
   spi_buf[0] = 0x10; // Power control register
   spi_buf[1] = 0x00;
   spi_buf[2] = 0x05; // PUA = PUB = 1
   wiringPiSPIDataRW(fspi_fd0, spi_buf, 3);
   
   // set DAC0 output
   spi_buf[0] = 0x00;
   spi_buf[1] = d >> 8;   // MSB
   spi_buf[2] = d & 0xFF; // LSB
   wiringPiSPIDataRW(fspi_fd0, spi_buf, 3);
#endif
}

void HWConnection::SetControlledValue(double value) {
   SetValve(value);
}

double HWConnection::GetValve() {
   return fValve;
}

void HWConnection::ReadTemperatures(unsigned int nSamples) {
   if (nSamples > fArraySize) nSamples = fArraySize;
   int ch = 0;
#ifdef __ARMEL__
   for (unsigned int sample = 0; sample < nSamples; ++sample) {
      
      double v[4];
      unsigned char spi_buf[4];
      
      spi_buf[0] = 0xC0;
      spi_buf[1] = 0x00;
      spi_buf[2] = 0x00;
      spi_buf[3] = 0x00; // Manual Ch 0 Conversion
      wiringPiSPIDataRW(fspi_fd1, spi_buf, 4);
      
      
      for (ch = 0 ; ch<4 ; ch++) {
         spi_buf[0] = 0xC0 + (ch+1)*0x04;
         spi_buf[1] = 0x00;
         spi_buf[2] = 0x00;
         spi_buf[3] = 0x00; // Manual Ch 'c' Conversion
         
         wiringPiSPIDataRW(fspi_fd1, spi_buf, 4);
         
         // convert to Volts
         v[ch] = ((spi_buf[2] << 8) | spi_buf[3])/65535.0 * 20.48 - 10.24;
         
         // round to three digit
         fTemperatureArray[ch][fIndex] = v[ch] * fConversionFactor[ch];
      }
      ++fIndex;
      if (fIndex == fArraySize) fIndex = 0;
   }
   
   //Average
   for (ch = 0; ch < 4; ++ch) {
      double sum = 0;
      for (unsigned int i = 0; i < fArraySize; ++i) {
         sum += fTemperatureArray[ch][i];
      }
      fTemperature[ch] = sum / fArraySize;
   }
#else
   for (ch = 0; ch < 4; ++ch) {
      fTemperature[ch] = 23 + ch;
   }
#endif
}

void HWConnection::ReadTemperatures() {
   int nEntries = fArraySize / (fIntegrationWindow * fCycles);
   ReadTemperatures(nEntries);
}

double HWConnection::GetTemperature(unsigned int channel) {
   return fTemperature[channel];
}
