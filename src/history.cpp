//
//  history.c
//  
//
//  Created by Patrick Schwendimann on 12.12.18.
//

#include "history.h"
#include "historyEntry.h"
#include <sys/time.h>
#include "main.h"
#include "hardwareConnection.h"
#include "pidControl.h"
#include "shared.h"
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <unistd.h>
#include <math.h>

History::History(sharedMemory* shared)
{
   fInterval = 0.9;
   fStatusInterval = 900;
   fCleanupInterval = 6 * 3600;
   struct timeval now;
   gettimeofday(&now, 0);
   fLastCall = now.tv_sec;
   fLastStatus = now.tv_sec;
   fLastCleanup = now.tv_sec;
   
   
   fHistory = shared->GetHistory();
   fHistorySize = shared->GetHistorySize();
   fHistoryStatPar = shared->GetHistoryStatusParameters();
   if (*gDEBUG) {
      int s = *fHistorySize % 60;
      int m = *fHistorySize / 60;
      int h = m / 60;
      int d = h / 24;
      m %= 60;
      h %= 24;
      
      printf("History Size: %ld (%dd %dh %dm %ds)\n",
             *fHistorySize, d, h, m, s);
   }
   
   historyEntry empty = {0, 0, 0, 0, 0, 0};
   for (long i = 0; i < *fHistorySize; ++i) {
      if (*gDEBUG > 1) {
         printf("%ld/%ld\n", i, *fHistorySize);
      }
      *(fHistory + i) = empty;
   }
   fCurrentIndex = shared->GetCurrentIndex();
   fCycleCompleted = shared->GetCycleCompleted();
   if (*gDEBUG) {
      printf("initialisation of history memory completed \n");
   }
}

History::~History()
{
   
}

void History::TimeStep()
{
   struct timeval now;
   gettimeofday(&now, 0);
   
   if (now.tv_sec - fLastCall > fInterval) {
      fLastCall = now.tv_sec;
      Write();
   }
   
   if (now.tv_sec - fLastStatus > fStatusInterval) {
      fLastStatus = now.tv_sec;
      WriteStatus();
   }
   
   if (now.tv_sec - fLastCleanup > fCleanupInterval) {
      fLastCleanup = now.tv_sec;
      CleanUp();
   }
   
}

void History::Write()
{
   historyEntry *anEntry = fHistory + *fCurrentIndex;
   anEntry->fTime = fLastCall;
   anEntry->fValve = fHWConn->GetValve();
   anEntry->fAirTemp = fHWConn->GetTemperature(0);
   anEntry->fHutTemp = fHWConn->GetTemperature(1);
   anEntry->fLM35Temp = fHWConn->GetTemperature(2);
   anEntry->fAirReqTemp = fAirControl->GetTemperature();
   anEntry->fHutReqTemp = fHutControl->GetTemperature();
   
   //Write to file
   struct tm *tmNow;
   
   char timeBuf[10];
   char dateBuf[16];
   char fileName[20];
   
   tmNow = localtime(&fLastCall);
   
   strftime(timeBuf, sizeof timeBuf, "%H:%M:%S", tmNow);
   strftime(dateBuf, sizeof dateBuf, "%Y-%m-%d", tmNow);
   sprintf(fileName, "log/%s.txt", dateBuf);
   
   FILE *historyFile = fopen(fileName, "r");
   
   if (not historyFile) {
      historyFile = fopen(fileName, "w");
      fprintf(historyFile, "Date: %s\n", dateBuf);
      fprintf(historyFile,
              "Time, Valve, Air Temp, Hut Temp, LM35 Temp, Target Air Temp, Target Hut Temp, Air Int, Hut Int\n");
   }
   fclose(historyFile);
   
   historyFile = fopen(fileName, "a");
   fprintf(historyFile, "%s, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n",
           timeBuf,
           anEntry->fValve,
           anEntry->fAirTemp,
           anEntry->fHutTemp,
           anEntry->fLM35Temp,
           anEntry->fAirReqTemp,
           anEntry->fHutReqTemp,
           fAirControl->GetIntegral(),
           fHutControl->GetIntegral());
   fclose(historyFile);
   
   if(*gDEBUG) {
      printf("Index: %d Entry written: %d %f %f %f %f %f %f\n",
             int(*fCurrentIndex),
             int(anEntry->fTime),
             anEntry->fValve,
             anEntry->fAirTemp,
             anEntry->fHutTemp,
             anEntry->fLM35Temp,
             anEntry->fAirReqTemp,
             anEntry->fHutReqTemp);
   }
   if (++(*fCurrentIndex) >= *fHistorySize) {
      //Once last position is reached, start over
      *fCurrentIndex = 0;
      *fCycleCompleted = true;
   }
}

std::vector<historyEntry> History::GetHistory(long nEntries, long tStart, long tEnd)
{
   if (*gDEBUG) {
      printf("History::GetHistory(%ld, %ld, %ld)\n", nEntries, tStart, tEnd);
   }
   std::vector<historyEntry> selection;
   long firstIndex = (*fCycleCompleted) ? *fCurrentIndex : 0;
   long lastIndex = *fCurrentIndex - 1;
   if (lastIndex == -1 && *fCycleCompleted) lastIndex = *fHistorySize - 1;
   if (*gDEBUG) {
      printf("first %ld, last %ld, current %ld, size %ld\n", firstIndex, lastIndex, *fCurrentIndex, *fHistorySize);
   }
   if (tStart < fHistory[firstIndex].fTime) {
      tStart = fHistory[firstIndex].fTime;
   }
   
   long time = tEnd - tStart;
   
   if (*gDEBUG) {
      printf("tStart %ld, tEnd %ld, time %ld\n", tStart, tEnd, time);
   }
   if (*gDEBUG > 1) {
      Dump(0,0,0);
   }
   
   
   double tNext = 0;
   double dt = 0;
   if (time) {
      dt = nEntries ? time * 1. / nEntries : 0;
      tNext = dt ? ceil(tEnd / dt) * dt : tEnd;
   } else if (lastIndex != -1){
      tStart = fHistory[firstIndex].fTime;
      tEnd = fHistory[lastIndex].fTime;
      dt = nEntries ? ceil((tEnd - tStart) * 1. / nEntries) : 0;
      tNext = dt ? ceil(tEnd / dt) * dt : tEnd;
   }
   
   //if nEntries == 0, get all Entries
   if (!nEntries) nEntries = (*fCycleCompleted) ? *fHistorySize : lastIndex - firstIndex;
   
   long index = lastIndex;
   if (*gDEBUG) {
      printf("GetHistory - mainloop\n");
   }
   
   //locate the first entry to use
   while (fHistory[index].fTime > tNext && index != firstIndex) {
      --index;
      if (index < 0) index += *fHistorySize;
   }
   
   //assert to be in the correct bin
   if (dt) {
      while (fHistory[index].fTime < tNext) tNext -= dt;
   }
   
   historyEntry avrgEntry = {0, 0, 0, 0, 0, 0};
   int nAve = 0;
   //mainloop
   while (fHistory[index].fTime > tStart && index != firstIndex) {
      avrgEntry.fTime = 0;
      avrgEntry.fValve = 0;
      avrgEntry.fAirTemp = 0;
      avrgEntry.fAirReqTemp = 0;
      avrgEntry.fHutTemp = 0;
      avrgEntry.fHutReqTemp = 0;
      avrgEntry.fLM35Temp = 0;
      nAve = 0;
      while (fHistory[index].fTime > tNext && index != firstIndex) {
         avrgEntry *= nAve / (nAve + 1.);
         ++nAve;
         avrgEntry += fHistory[index] / nAve;
         --index;
         if (index < 0) {
            if (*gDEBUG) {
               printf ("index < 0\n");
            }
            index += *fHistorySize;
         }
         if (*gDEBUG) {
            printf("begin index %ld\n", index);
         }
      }
      if (nAve) {
         if (*gDEBUG) {
            printf("nAve %d, avrgEntry (%ld, %f, %f, %f, %f, %f, %f)\n",
                   nAve,
                   avrgEntry.fTime,
                   avrgEntry.fValve,
                   avrgEntry.fAirTemp,
                   avrgEntry.fAirReqTemp,
                   avrgEntry.fHutTemp,
                   avrgEntry.fHutReqTemp,
                   avrgEntry.fLM35Temp);
         }
         selection.push_back(avrgEntry);
      } else {
         selection.push_back(fHistory[index]);
      }
      
      if (index != firstIndex) {
         if (dt) {
            while (fHistory[index].fTime <= tNext) {
               tNext -= dt;
            }
         } else {
            --index;
            if (index < 0) {
               index += *fHistorySize;
            }
            if (*gDEBUG) {
               printf("end index %ld\n", index);
            }
         }
      }
   }
   if (*gDEBUG) {
      printf("End of History::GetHistory\n");
   }
   return selection;
}

std::vector<historyEntry> History::GetHistory(long nEntries, long time) {
   struct timeval now;
   gettimeofday(&now, 0);
   return GetHistory(nEntries, now.tv_sec - time, now.tv_sec);
}

void History::Dump(long nEntries, long time, bool toFile = false)
{
   struct timeval now;
   gettimeofday(&now, 0);
   double tNext;
   double dt;
   FILE* dumpFile = 0;
   
   long firstIndex = (*fCycleCompleted) ? *fCurrentIndex : 0;
   long lastIndex = *fCurrentIndex - 1;
   if (lastIndex == -1 && *fCycleCompleted) lastIndex = *fHistorySize - 1;
   
   if (time) {
      tNext = now.tv_sec - time;
      dt = nEntries ? time * 1. / nEntries : 0;
   } else {
      tNext = (lastIndex != -1) ? fHistory[firstIndex].fTime : 0;
      dt = nEntries ? (now.tv_sec - tNext) * 1. / nEntries : 0;
   }
   
   //if nEntries == 0, get all Entries
   if (!nEntries) nEntries = (*fCycleCompleted) ? *fHistorySize : lastIndex - firstIndex;
   
   if (toFile) {
      dumpFile = fopen("HistoryDump.txt", "w");
   }
   
   struct tm *aTime = 0;
   char timeBuf[10];
   char dateBuf[16];
   if (toFile) {
      fprintf(dumpFile, "Index\tDate\t\tTime\t\tValve\t\tT air\t\tT air req\t T hut\t\t T hut req\t\tT LM35\n");
   } else {
      printf("Index\tDate\t\tTime\t\tValve\t\tT air\t\tT air req\t T hut\t\t T hut req\t\tT LM35\n");
   }
   
   long i = firstIndex;
   for (long iEntries = 0; (iEntries < nEntries) && (i != firstIndex || iEntries == 0); ++iEntries) {
      while (i != lastIndex && fHistory[i].fTime < tNext) {
         ++i;
         if (i >= *fHistorySize) {
            i -= *fHistorySize;
         }
      }
      aTime = localtime(&fHistory[i].fTime);
      
      strftime(timeBuf, sizeof timeBuf, "%H:%M:%S", aTime);
      strftime(dateBuf, sizeof dateBuf, "%Y-%m-%d", aTime);
      if (toFile) {
         fprintf(dumpFile, "%ld \t %s \t %s \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f\n",
                 i,
                 dateBuf,
                 timeBuf,
                 fHistory[i].fValve,
                 fHistory[i].fAirTemp,
                 fHistory[i].fAirReqTemp,
                 fHistory[i].fHutTemp,
                 fHistory[i].fHutReqTemp,
                 fHistory[i].fLM35Temp);
      } else {
         printf("%ld \t %s \t %s \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f\n",
                i,
                dateBuf,
                timeBuf,
                fHistory[i].fValve,
                fHistory[i].fAirTemp,
                fHistory[i].fAirReqTemp,
                fHistory[i].fHutTemp,
                fHistory[i].fHutReqTemp,
                fHistory[i].fLM35Temp);
      }
      if (dt) {
         while (fHistory[i].fTime >= tNext) tNext += dt;
      } else {
         ++i;
         if (i >= *fHistorySize) {
            i -= *fHistorySize;
         }
      }
      
   }
   if (!toFile) {
      printf("----\n");
   }
}

void History::ReadFromFiles()
{
   struct timeval now;
   gettimeofday(&now, 0);
   
   int h, m, s;
   historyEntry *anEntry = 0;
   int nRead = 0;
   long t;
   
   for (int i = -7; i <=0; ++i) {
      //read back the last 7 days if possible
      tm *theDateTime;
      
      char dateBuf[16];
      char fileName[20];
      
      t = now.tv_sec + i * 86400;
      theDateTime = localtime(&t);
      
      strftime(dateBuf, sizeof dateBuf, "%Y-%m-%d", theDateTime);
      sprintf(fileName, "log/%s.txt", dateBuf);
      if (*gDEBUG) {
         printf("try file: %s", fileName);
      }
      std::ifstream infile(fileName);
      std::string line;
      if (infile.is_open() && infile.good()) {
         if (*gDEBUG) {
            printf(" ... found\n");
         }
         while (getline(infile, line)) {
            anEntry = &fHistory[*fCurrentIndex];
            if (*gDEBUG > 1) {
               printf("Read line: %s\n", line.c_str());
            }
            nRead = sscanf(line.c_str(), "%d:%d:%d, %lf, %lf, %lf, %lf, %lf, %lf",
                           &h, &m, &s,
                           &(anEntry->fValve),
                           &(anEntry->fAirTemp),
                           &(anEntry->fHutTemp),
                           &(anEntry->fLM35Temp),
                           &(anEntry->fAirReqTemp),
                           &(anEntry->fHutReqTemp)
                           );
            if (nRead >= 8) {
               theDateTime->tm_hour = h;
               theDateTime->tm_min = m;
               theDateTime->tm_sec = s;
               anEntry->fTime = mktime(theDateTime);
               if (nRead == 8) anEntry->fLM35Temp = 0;
               ++(*fCurrentIndex);
               if (*fCurrentIndex >= *fHistorySize) {
                  *fCurrentIndex = 0;
                  *fCycleCompleted = true;
               }
               if (*gDEBUG > 1) {
                  char timeBuf[16];
                  
                  tm *aTime = localtime(&(anEntry->fTime));
                  
                  strftime(timeBuf, sizeof timeBuf, "%H:%M:%S", aTime);
                  strftime(dateBuf, sizeof dateBuf, "%Y-%m-%d", aTime);
                  
                  printf("%ld: Read vals: %s, %.1f, %.1f, %.1f, %.1f, %.1f, %.1f\n",
                         *fCurrentIndex,
                         timeBuf,
                         anEntry->fValve,
                         anEntry->fAirTemp,
                         anEntry->fHutTemp,
                         anEntry->fLM35Temp,
                         anEntry->fAirReqTemp,
                         anEntry->fHutReqTemp);
               }
            } else if (*gDEBUG > 1) {
               printf("nRead: %i \n", nRead);
            }
         }
      } else if (*gDEBUG) {
         printf(" ... not found \n");
      }
   }
}

int History::WriteStatus()
{
   FILE* aFile = 0;
   if (gSIG) {
      aFile = fopen("termStatus.txt", "w");
   } else {
      aFile = fopen("runStatus.txt", "w");
   }
   
   if (not aFile) {
      int i = 0;
      char fileBuf[32];
      while (not aFile and i < 20) {
         sprintf(fileBuf, "%sStatus%02i.txt", gSIG ? "term":"run", ++i);
         aFile = fopen(fileBuf, "w");
      }
   }
   if (aFile) {
      struct timeval now;
      gettimeofday(&now, 0);
      struct tm *tmNow = localtime(&now.tv_sec);
      char tmBuf[32];
      strftime(tmBuf, sizeof tmBuf, "%Y-%m-%d %H:%M:%S", tmNow);
      
      fprintf(aFile, "time = %ld (%s)\n", now.tv_sec, tmBuf);
      fprintf(aFile, "sig = %d\n", gSIG);
      fprintf(aFile, "cycles = %d\n", fHWConn->GetCycles());
      fprintf(aFile, "integration = %d\n", fHWConn->GetIntegrationWindow());
      fprintf(aFile, "airActive = %d\n", fAirControl->GetActive());
      fprintf(aFile, "airChannel = %d\n", fAirControl->GetChannel());
      fprintf(aFile, "airP = %f\n", fAirControl->GetP());
      fprintf(aFile, "airI = %f\n", fAirControl->GetI());
      fprintf(aFile, "airD = %f\n", fAirControl->GetD());
      fprintf(aFile, "airIntegral = %f\n", fAirControl->GetIntegral());
      fprintf(aFile, "hutActive = %d\n", fHutControl->GetActive());
      fprintf(aFile, "hutChannel = %d\n", fHutControl->GetChannel());
      fprintf(aFile, "hutP = %f\n", fHutControl->GetP());
      fprintf(aFile, "hutI = %f\n", fHutControl->GetI());
      fprintf(aFile, "hutD = %f\n", fHutControl->GetD());
      fprintf(aFile, "hutIntegral = %f\n", fHutControl->GetIntegral());
      fprintf(aFile, "valve = %f\n", fHWConn->GetValve());
      fprintf(aFile, "airReqTemp = %f\n", fAirControl->GetTemperature());
      fprintf(aFile, "hutReqTemp = %f\n", fHutControl->GetTemperature());
      fprintf(aFile, "histStatLow = %f\n", fHistoryStatPar->low);
      for (int i = 0; i < NUM_HIST_STAT; ++i) {
         fprintf(aFile, "statTimespan%d = %f\n", i, fHistoryStatPar->timespan[i]);
         fprintf(aFile, "statUpLimit%d = %f\n", i, fHistoryStatPar->upLim[i]);
      }
      fclose(aFile);
      return 0;
   } else {
      return 1;
   }
}

void History::ReadStatus(char* fileBuf)
{
   std::ifstream infile;
   std::string line;
   
   if (*fileBuf) {
      //a file name was passed
      printf("using specified status\n");
      infile.open(fileBuf, std::ifstream::in);
   } else {
      //Try to find the latest status file in current folder
      DIR *dir = opendir("./");
      dirent *ent;
      dirent *latestEnt = 0;
      struct stat fileStat;
      long latestTime = 0;
      
      while ((ent = readdir(dir))) {
         if (strstr(ent->d_name, "Status") && strstr(ent->d_name, ".txt\0")) {
            if (*gDEBUG) {
               printf("Candidate file: %s\n", ent->d_name);
            }
            stat(ent->d_name, &fileStat);
            if (fileStat.st_mtime > latestTime) {
               latestTime = fileStat.st_mtime;
               latestEnt = ent;
            }
         } else if (*gDEBUG) {
            printf("Not a Candidate: %s\n", ent->d_name);
         }
      }
      if (latestEnt) {
         printf("found potential file\n");
         sprintf(fileBuf,"%s", latestEnt->d_name);
         infile.open(fileBuf, std::ifstream::in);
      }
   }
   
   char argName[32];
   int argInt;
   double argVal;
   
   if (infile.good() && infile.is_open()) {
      printf("Reading file \"%s\"\n", fileBuf);
      while (getline(infile, line)) {
         if (*gDEBUG) {
            printf("%s\n", line.c_str());
         }
         if (sscanf(line.c_str(), "%s = %lf", argName, &argVal) == 2) {
            // single argument parametersuu
            if (!strcmp(argName, "cycles")) {
               fHWConn->SetCycles((int)argVal);
               printf("Readout cycles per sec: %d\n", (int)argVal);
            } else if (!strcmp(argName, "integration")) {
               fHWConn->SetIntegrationWindow((int)argVal);
               printf("Integration Window: %d\n", (int)argVal);
            } else if (!strcmp(argName, "airActive")) {
               fAirControl->SetActive((int)argVal);
               printf("Air Control Active: %d\n", (int)argVal);
            } else if (!strcmp(argName, "airChannel")) {
               fAirControl->SetChannel((int)argVal);
               printf("Air Channel: %d\n", (int)argVal);
            } else if (!strcmp(argName, "airP")) {
               fAirControl->SetP(argVal);
               printf("Air P: %f\n", argVal);
            } else if (!strcmp(argName, "airI")) {
               fAirControl->SetI(argVal);
               printf("Air I: %f\n", argVal);
            } else if (!strcmp(argName, "airD")) {
               fAirControl->SetD(argVal);
               printf("Air D: %f\n", argVal);
            } else if (!strcmp(argName, "airIntegral")) {
               fAirControl->SetIntegral(argVal);
               printf("Air Integral: %f\n", argVal);
            } else if (!strcmp(argName, "hutActive")) {
               fHutControl->SetActive((int)argVal);
               printf("Hut Control Active: %d\n", (int)argVal);
            } else if (!strcmp(argName, "hutChannel")) {
               fHutControl->SetChannel((int)argVal);
               printf("Hut Channel: %d\n", (int)argVal);
            } else if (!strcmp(argName, "hutP")) {
               fHutControl->SetP(argVal);
               printf("Hut P: %f\n", argVal);
            } else if (!strcmp(argName, "hutI")) {
               fHutControl->SetI(argVal);
               printf("Hut I: %f\n", argVal);
            } else if (!strcmp(argName, "hutD")) {
               fHutControl->SetD(argVal);
               printf("Hut D: %f\n", argVal);
            } else if (!strcmp(argName, "hutIntegral")) {
               fHutControl->SetIntegral(argVal);
               printf("Hut Integral: %f\n", argVal);
            } else if (!strcmp(argName, "valve")) {
               fHWConn->SetValve(argVal);
               printf("Valve: %f\n", argVal);
            } else if (!strcmp(argName, "airReqTemp")) {
               fAirControl->SetTemperature(argVal);
               printf("Air requested temperature: %f\n", argVal);
            } else if (!strcmp(argName, "hutReqTemp")) {
               fHutControl->SetTemperature(argVal);
               printf("Hut requested temperature: %f\n", argVal);
            } else if (!strcmp(argName, "histStatLow")) {
               fHistoryStatPar->low = argVal;
               printf("Status Lower Limit: %f\n", argVal);
            } else if (sscanf(argName, "statTimespan%d", &argInt)) {
               if (argInt >= 0 && argInt < NUM_HIST_STAT) {
                  fHistoryStatPar->timespan[argInt] = argVal;
                  printf("Status Time Span %d: %f\n", argInt, argVal);
               }
            } else if (sscanf(argName, "statUpLimit%d", &argInt)) {
               if (argInt >= 0 && argInt < NUM_HIST_STAT) {
                  fHistoryStatPar->upLim[argInt] = argVal;
                  printf("Status Upper Limit %d: %f\n", argInt, argVal);
               } 
            }  else  if (*gDEBUG) {
               printf("ignoring parameter: %s\n", argName);
            }
         }
      }
   } else {
      printf("no suitable status file found\n");
   }
}

void History::CleanUp()
{
   //Cleanup old files if more than 80% of disk is used
   //Get Disk Space:
   struct statvfs fs;
   dirent *ent;
   dirent *oldestEnt = 0;
   struct stat fileStat;
   long latestTime = LONG_MAX;
   char fileBuf[32];
   
   
   statvfs("./log", &fs);
   if (*gDEBUG) {
#ifdef __ARMEL__
      printf("Available Disk Space: %lu / %lu  %1.2f\n",
             fs.f_bavail, fs.f_blocks, 1. * fs.f_bavail / fs.f_blocks);
#else
      printf("Available Disk Space: %u / %u  %1.2f\n",
             fs.f_bavail, fs.f_blocks, 1. * fs.f_bavail / fs.f_blocks);
#endif
   }
   while (1. * fs.f_bavail / fs.f_blocks < 0.2) {
      DIR *dir = opendir("./log");
      
      while ((ent = readdir(dir))) {
         if (ent->d_name[0] == '.') continue;
         stat(ent->d_name, &fileStat);
         if (fileStat.st_mtime < latestTime) {
            latestTime = fileStat.st_mtime;
            oldestEnt = ent;
         }
      }
      if (oldestEnt) {
         //an old file - delete to free some space
         sprintf(fileBuf, "log/%s", oldestEnt->d_name);
         if (*gDEBUG) {
            printf("Removing Oldest File: %s\n", fileBuf);
         }
         remove(fileBuf);
      }
      
      statvfs("./log", &fs);
      closedir(dir);
   }
}

historyEntry History::GetLastEntry()
{
   historyEntry retVal = {0, 0, 0, 0, 0, 0};
   if (*fCurrentIndex != 0 || *fCycleCompleted) {
      long indx = *fCurrentIndex ? *fCurrentIndex - 1 : *fHistorySize -1;
      retVal = fHistory[indx];
   }
   return retVal;
}
