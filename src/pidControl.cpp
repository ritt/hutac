#include "pidControl.h"
#include "hardwareConnection.h"
#include "main.h"
#include "shared.h"
#include <stdio.h>
#include <math.h>


pidControl::pidControl(short controlId,
                       control* controlled,
                       HWConnection* conn,
                       sharedMemory* shared)
{
   printf("initialise class pid control %i\n", controlId);
   fControlId = controlId;
   fControlled = controlled;
   fConnection = conn;
   
   fParameters = shared->GetPIDpars(controlId);
   
   clock_gettime(CLOCK_MONOTONIC, &fLastCallTime);
   fLastDiff = 0;
}

int pidControl::pidStep()
{
   if (fParameters->isActive) {
      // Update variables
      //-------------------
      
      struct timespec callTime;
      clock_gettime(CLOCK_MONOTONIC, &callTime);
      
      bool integrate = true;
      
      double timeElapsed = callTime.tv_sec - fLastCallTime.tv_sec;
      timeElapsed += 1e-9 * (callTime.tv_nsec - fLastCallTime.tv_nsec) ;
      
      if (timeElapsed < 0.99) return -1;

      // Avoid excessive time steps. Occure likely due to server issue or pausing a control
      if (timeElapsed > 2) timeElapsed = 1; 
      
      
      double newDiff = fParameters->reqTemp - fConnection->GetTemperature(fParameters->channel);
      double newInt = fParameters->intDiff;
      double derivative = (newDiff - fLastDiff) / timeElapsed;

      if (newDiff > 0.06 || newDiff < -0.06 || fParameters->channel == 2) {
         newInt += 0.5 * (fLastDiff + newDiff) * timeElapsed;
      }
      
      double valueToSet = 0;
      //proportional part:
      valueToSet += fParameters->P * newDiff;
      //integral part:
      valueToSet += fParameters->I * newInt;
      //Differential part:
      valueToSet += fParameters->D * derivative;
      
      // Commit actions
      //-------------------
      if (*gDEBUG) {
         printf("PID Step: id = %i, ch = %d, fProp = %lf, fInt = %lf, fDiff = %lf",
                fControlId, fParameters->channel, fParameters->P, fParameters->I, fParameters->D);
         printf("newDiff = %f, newInt = %lf, vts = %lf\n",
                newDiff, newInt, valueToSet);
      }
      switch (fControlId) {
         case 0:
            //air control - set the valve
            //value 0: valve closed, no cooling
            //value 100: valve open, maximal cooling
            fControlled->SetControlledValue(50 - valueToSet);
            if ((valueToSet > 50 && newInt > fParameters->intDiff) || (valueToSet < -50 && newInt < fParameters->intDiff)) {
               //Prevent integration windup
               integrate = false;
            }
            break;
         case 1:
            //Hut control - set requested temperatur for the cold air 
            // Adjust temperature of cool air.
            fControlled->SetControlledValue(fParameters->reqTemp + valueToSet);
            if ((valueToSet + newDiff > 3 && newInt > fParameters->intDiff) || (fParameters->reqTemp + valueToSet < 14 && newInt < fParameters->intDiff)) {
               //Prevent integration windup
               //Do not integrate if the requested cool air temperature is
               // - above current hut temperature + 3
               // - below 14°C 
               integrate = false;
            }
            break;
      }
      
      // Save new values
      //-------------------
      
      fLastCallTime = callTime;
      fLastDiff = newDiff;
      if (integrate) {
         fParameters->intDiff = newInt;
      }
      return 0;
   } else {
      return 1;
   }
}

void pidControl::SetControlledValue(double newValue)
{
   SetTemperature(newValue);
}
