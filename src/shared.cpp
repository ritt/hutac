#include "shared.h"
#include "main.h"
#include "history.h"
#include "pidControl.h"
#include <unistd.h>



sharedMemory::sharedMemory(bool mode) {
   using namespace boost::interprocess;
   fIsMaster = mode;

   printf("Initialising shared memory in mode %d\n", mode);
   //Memory size needed:
   long size = 0;
   //general flags:
   size += sizeof(int); //gDEBUG,
   
   size += sizeof(int) + sizeof(float); //command + 1 argument (e.g. SetValve)
   
   //pid control
   size += 2 * sizeof(pidPars); //2 PID controls

   //history
   size += 2 * sizeof(long); //fCurrentIndex, fHistorySize;
   size += sizeof(int); //fCycleCompleted
   
   size += sizeof(historyStatusParameters); // Status parameters

   float totMem = 0.3 * float(sysconf(_SC_PHYS_PAGES)) * float(sysconf(_SC_PAGE_SIZE));
   long histSize = 60 * 24 * 3600; // 60 days
   if (0.3 * totMem / sizeof(historyEntry) < histSize) {
      histSize = 0.3 * totMem / sizeof(historyEntry);
   }
   size += histSize * sizeof(historyEntry);
   
   printf("Size of shared memory: %ld\n", size);
   fShared = 0;
   if (fIsMaster) {
      //Allocate the memory
      shared_memory_object::remove("hutac");
      fShared = new shared_memory_object(create_only, "hutac", read_write);
      fShared->truncate(size);
   } else {
      fShared = new shared_memory_object(open_only, "hutac", read_write);
   }
   fReg = new mapped_region(*fShared, read_write);
   
   printf("shared memory allocated\n");
   //Start allocating things
   //basic types first
   
   char* allocator = static_cast<char*>(fReg->get_address());
   fDebug = reinterpret_cast<int*>(allocator);
   allocator += sizeof(int);
   fCommand = reinterpret_cast<int*>(allocator);
   allocator +=sizeof(int);
   fArgument = reinterpret_cast<float*>(allocator);
   allocator += sizeof(float);
   fCurrentIndex = reinterpret_cast<long*>(allocator);
   allocator += sizeof(long);
   fHistorySize = reinterpret_cast<long*>(allocator);
   allocator += sizeof(long);
   fCycleCompleted = reinterpret_cast<bool*>(allocator);
   allocator += sizeof(int);

   fPID0 = reinterpret_cast<pidPars*>(allocator);
   allocator += sizeof(pidPars);
   fPID1 = reinterpret_cast<pidPars*>(allocator);
   allocator += sizeof(pidPars);
   fHistoryStatPars = reinterpret_cast<historyStatusParameters*>(allocator);
   allocator += sizeof(historyStatusParameters);
   fHistory = reinterpret_cast<historyEntry*>(allocator);
   
   printf("Pointers allocated:\n");
   
   if (fIsMaster) {
      *fDebug = 0;
      *fCommand = 0;
      *fArgument = 0;
      *fHistorySize = histSize;
      gDEBUG = fDebug;
   }
}

sharedMemory::~sharedMemory() {
   using namespace boost::interprocess;
   if (fIsMaster) {
      shared_memory_object::remove("hutac");
   }
   delete fReg;
   delete fShared;
}

pidPars* sharedMemory::GetPIDpars(int pidID) {
   if (pidID == 0) {
      return fPID0;
   } else if (pidID == 1) {
      return fPID1;
   } else {
      return NULL;
   }
}

void sharedMemory::SetCommand(int cmd, float arg) {
   while (cmd && *fCommand) {
      usleep(10000);
   }
   *fCommand = cmd;
   *fArgument = arg;
}
