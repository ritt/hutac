
// main.c
// PID control tool
// used for temperatur control of detector hut
// Authors:
//   Patrick Schwendimann (patrick.schwendimann@psi.ch)
//
// Based on code by Stefan Ritt

#include <stdio.h>
#include <iostream>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "main.h"
#include "hardwareConnection.h"
#include "pidControl.h"
#include "shared.h"
#include "history.h"

int gSIG = 0;
int *gDEBUG = 0;

void sigHandler(int SIGNUM) {
   //A most general handler
   //Any SIGNUM different from 1 will terminate the mainloop
   if (SIGNUM == SIGHUP) {
      //ignore a hangup due to unexpected disconnection from terminal
   } else {
      printf("received signal %d, preparing to terminate\n", SIGNUM);
      gSIG = SIGNUM;
   }
}

int main(int argc, char *argv[])
{
   
   //Initialise the hardware connection
   //This has to happen first because some reasons?
   HWConnection hwConn;
   
   //Initialise shared memory
   sharedMemory shared(1);
   shared.GetDebug(gDEBUG);
   printf("shared memory initialised\n");
      
   //Prepare signal handling
   signal(SIGHUP, sigHandler);
   signal(SIGINT, sigHandler);
   signal(SIGTERM, sigHandler);
   
   char fileBuf[32] = "\0";
   
   for (int iarg = 0; iarg < argc; ++iarg) {
      if (not strcmp(argv[iarg], "-d")) {
         *gDEBUG = 1;
      } else if (not strcmp(argv[iarg], "-D")) {
         *gDEBUG = 2;
      } else if (not strcmp(argv[iarg], "-s")) {
         ++iarg;
         sscanf(argv[iarg], "%s", fileBuf);
      }
   }
  
   
   //Initialise PID control
   pidControl pidAirControl(0, &hwConn, &hwConn, &shared);
   pidControl pidHutControl(1, &pidAirControl, &hwConn, &shared);
   
   History history(&shared);
   history.SetHardwareConnection(&hwConn);
   history.SetAirControl(&pidAirControl);
   history.SetHutControl(&pidHutControl);
   
   history.ReadFromFiles();
   history.ReadStatus(fileBuf);
   history.CleanUp();
   
   printf("initialisation completed, entering running mode\n");
   std::flush(std::cout);

   while(not gSIG) {
      for (unsigned int i = 0; i < hwConn.GetCycles(); ++i) {
         usleep(1000000 / hwConn.GetCycles());
         hwConn.ReadTemperatures();
      }
      if (*gDEBUG > 1) {
	 printf("PID step:\n");
      }
      pidHutControl.pidStep();
      pidAirControl.pidStep();
      if (shared.GetCommand() == 1) {
         //Write current status
         history.WriteStatus();
         shared.SetCommand(0);
      } else if (shared.GetCommand() == 2) {
         //Set Valve
         hwConn.SetValve(shared.GetArgument());
         history.WriteStatus();
         shared.SetCommand(0);
      }
      history.TimeStep();
   }
   
   history.WriteStatus();
   return gSIG;
}
