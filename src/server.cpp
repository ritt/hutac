
// server.c - Simple JSON-RPC based server
// used for temperatur control of detector hut
// Authors:
//   Patrick Schwendimann (patrick.schwendimann@psi.ch)
//
// Based on code by Stefan Ritt

#include <stdio.h>
#include <iostream>
#include <signal.h>
#include "mongoose.h"

#include "server.h"
#include "rpcMethods.h"
#include "historyReader.h"
#include "shared.h"

static struct mg_serve_http_opts s_http_server_opts;
const char *s_http_port = "8080";
int *gDEBUG = new int;
int gSIG = 0;

void sigHandler(int SIGNUM) {
   //A most general handler
   //Any SIGNUM different from 1 will terminate the mainloop
   if (SIGNUM == SIGHUP) {
      //ignore a hangup due to unexpected disconnection from terminal
   } else {
      printf("Server: received signal %d, preparing to terminate\n", SIGNUM);
      gSIG = SIGNUM;
   }
}

int main(int argc, char *argv[])
{
   *gDEBUG = 0;
   sharedMemory shared(0);
   signal(SIGHUP, sigHandler);
   signal(SIGINT, sigHandler);
   signal(SIGTERM, sigHandler);
   
   for (int iarg = 0; iarg < argc; ++iarg) {
      if (not strcmp(argv[iarg], "-d")) {
        *gDEBUG = 1;
      } else if (not strcmp(argv[iarg], "-D")) {
        *gDEBUG = 2;
      }
   }
   
   // Initialise the Server
   struct mg_mgr mgr;
   struct mg_connection *nc;
  
   //Initialise the hardware connection
   RPC rpc(&shared);
   HistoryReader history(&shared);
   
   mg_mgr_init(&mgr, NULL);
   nc = mg_bind(&mgr, s_http_port, RPC::ev_handler);
   if (nc == NULL) {
      fprintf(stderr, "Error starting server on port %s\n", s_http_port);
      exit(1);
   }
   
   mg_register_http_endpoint(nc, "/curl", RPC::curlHandler);
   
   mg_set_protocol_http_websocket(nc);
   s_http_server_opts.document_root = ".";
   s_http_server_opts.enable_directory_listing = "yes";
   
   printf("Server: Starting server on port %s\n", s_http_port);
   
   //Initialise PID control
   rpc.SetHistory(&history);
   
   printf("Server: initialisation completed, entering mainloop\n");
   std::flush(std::cout);
   while(not gSIG) {
      mg_mgr_poll(&mgr, 1000);
   }
   printf("Server: terminating server with signal %d\n", gSIG);
   return gSIG;
}
