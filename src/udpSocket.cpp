// Simple UDP socket server to read out the latest temperatures
// from the MEG II detector hut PID control.
// 
// Author: Patrick Schwendimann
//

#include <iostream>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>

#include "shared.h"
#include "historyReader.h"
#include "pidControl.h"

int gSIG = 0;
int *gDEBUG = new int(0);

void sigHandler(int SIGNUM) {
   //a most general handler
   // Any SIGNUM different from SIGHUP will terminate the mainloop
   if (SIGNUM == SIGHUP) {
      // ignore a hangup due to being disconnected from the terminal
   } else {
      printf("UDP socket received signal %d, terminating\n", SIGNUM);
      gSIG = SIGNUM;
   }
}

bool isIn(char c, std::string str) {
   for (unsigned int i = 0; i < str.size(); ++i) {
      if (c == str[i]) return true;
   }
   return false;
}

std::vector<std::string> readPacket(char* rawBuffer, int maxLen) {
   std::vector<std::string> result;
   std::string aString;
   for (char *c = rawBuffer; c < rawBuffer + maxLen; ++c) {
      if (isIn(*c, std::string(" ,;\n\r\0", 6))) {
         if (aString.size() > 0) {
            result.push_back(aString);
            aString.clear();
         }
      } else {
         aString.push_back(*c);
      }
      if (*c == 0) {
         // end of string character read.
         break;
      }
   }
   return result;
}

bool isEqual(std::string str1, std::string str2) {
   if (str1.size() != str2.size()) {
      return false;
   }
   for (unsigned int i = 0; i < str1.size(); ++i) {
      if (tolower(str1[i]) != tolower(str2[i])) {
         return false;
      }
   }
   return true;
}

int main(int argc, char** argv) {

   char rawBuffer[1024] = {0};
   std::vector<std::string> buffer;
   std::string reply;
   double newVal = 0;
   int port = 3000;

   int sock_fd;


   // Initialise shared memory and history reader
   sharedMemory shared(0);
   HistoryReader history(&shared);

   // create socket

   struct sockaddr_in sAddress;
   memset(&sAddress, 0, sizeof(sAddress));

   sAddress.sin_family = AF_INET;
   sAddress.sin_port   = htons(port);
   sAddress.sin_addr.s_addr = htonl(INADDR_ANY);

   sock_fd = socket(PF_INET, SOCK_DGRAM, 0);
   if (sock_fd < 0) {
      std::cerr << "could not create socket" << std::endl;
      exit(1);
   }

   if ((bind(sock_fd, (struct sockaddr*)&sAddress, sizeof(sAddress))) < 0) {
      std::cerr << "could not bind socket" << std::endl;
      exit(1);
   }

   struct sockaddr_in cAddress;
   socklen_t cAddrLen = sizeof(cAddress);

   while (gSIG == 0) {
      // clear buffer
      memset(rawBuffer, 0, sizeof(rawBuffer));
      // read the message in the inbound packet
      recvfrom(sock_fd, rawBuffer, sizeof(rawBuffer), 0, (struct sockaddr*)&cAddress, &cAddrLen);
      if (*gDEBUG) printf("Received >%s< from %s\n", rawBuffer, inet_ntoa(cAddress.sin_addr));
      buffer = readPacket(rawBuffer, sizeof(rawBuffer));
      for (unsigned int index = 0; index < buffer.size(); ++index) {
         reply.clear();
         if (isEqual(buffer[index], "READ")) {
            // create a reply
            historyEntry *anEntry = history.GetLastEntry();
            reply = std::to_string(anEntry->fValve) + "\n";
            reply += std::to_string(anEntry->fAirTemp) + "\n";
            reply += std::to_string(anEntry->fAirReqTemp) + "\n";
            reply += std::to_string(anEntry->fHutTemp) + "\n";
            reply += std::to_string(anEntry->fHutReqTemp) + "\n";
            reply += std::to_string(anEntry->fLM35Temp) + "\n";
            reply += std::to_string(history.GetOffsetStatus()) + "\n";
         } else if (isEqual(buffer[index], "START")) {
            shared.GetPIDpars(0)->isActive = true;
            shared.GetPIDpars(1)->isActive = true;
            reply = "done\n";
         } else if (isEqual(buffer[index], "STOP")) {
            shared.GetPIDpars(0)->isActive = false;
            shared.GetPIDpars(1)->isActive = false;
            shared.SetCommand(2, 0);
            reply = "done\n";
         } else if (isEqual(buffer[index], "SET")) {
            try {
               newVal = std::stod(buffer[index + 1]);
               if (newVal <= 10) {
                  reply = buffer[index] + " " + buffer[index + 1] + ": ";
                  reply += "No valid number above 10 °C could be extracted from " + buffer[index + 1] + "\n";
               } else {
                  shared.GetPIDpars(1)->reqTemp = newVal;
                  reply = "done\n";
               }
               ++index;
            }
            catch (std::invalid_argument&) {
               reply = buffer[index] + " " + buffer[index + 1] + ": ";
               reply += "No conversion to number possible: " + buffer[index + 1] + "\n";
            }
         } else {
            reply = "Unkown Request: " + buffer[index] + "\n";
            reply += "\nAvailable Commands:\n";
            reply += "READ         Return latest values for Valve, Air Temp,\n";
            reply += "             Req. Air Temp, Hut Temp, Req. Hut Temp, LM35 Temp\n";
            reply += "START        Start the PID control\n";
            reply += "STOP         Stop PID control, close the valve\n";
            reply += "SET T        Set the requested hut temperature to T\n";
         }
         if (*gDEBUG) {
            std::cout << "reply <" << reply << ">\n";
            std::flush(std::cout);
            std::flush(std::cerr);
         }
         sendto(sock_fd, reply.c_str(), strlen(reply.c_str()), 0, (struct sockaddr*)&cAddress, sizeof(cAddress));
      }
   }
   return 0;
}



